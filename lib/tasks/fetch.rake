namespace :fetch do
  desc "Fetch planets"
  task planets: :environment do
    service = AwstatsPools.new
    stats = service.all
    return if stats.empty?

    Planet.all.each do |planet|
      planet.update(stats.fetch(planet.name, {}))
    end

    PlanetsCache.write

    # Rails.logger.info("Planets stats request")
    # stats = WaxStats.new.get
    # Rails.logger.info("Planet stats received: #{stats}")
    #
    # stats.each do |stat|
    #   Planet.where(name: stat[:planet]).update_all(current_mining_pot: stat[:pot].to_f, stacked: stat[:stacked].to_i)
    # end
    #
    # Rails.logger.info("Planet stats were updated")
    #
    # PlanetsCache.write
  end

  desc "Fetch plots"
  task plots: :environment do
    land_stats = AwstatsLands.new.lands

    planets = Planet.all
    rarities = Rarity.all
    lands = Land.all

    land_stats.each do |land_stat|
      planet_name, rarity_name, mint, description, charge, mining, pow, nft, commission, x, y, user, id = land_stat
      land_name = description.split(' on ').first.delete(' ').underscore
      land = lands.find { |l| l.name == land_name }
      planet = planets.find { |p| p.name == planet_name.underscore }
      rarity = rarities.find { |r| r.name ==  rarity_name.underscore}

      plot = Plot.where(planet: planet, coordinate_x: x, coordinate_y: y).first_or_initialize
      plot.attributes = {
        planet: planet,
        land:   land,
        rarity: rarity,
        coordinate_x: x,
        coordinate_y: y,
        owner_profit_share_from_mining: commission,
        land_owned_by: user
      }
      plot.save
    end

    PlotsCache.write
  end
end
