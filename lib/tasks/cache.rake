namespace :cache do
  require_relative '../../app/caches/model_cache'
  require_relative '../../app/caches/planets_cache'
  require_relative '../../app/caches/lands_cache'
  require_relative '../../app/caches/plots_cache'
  require_relative '../../app/caches/rarities_cache'
  require_relative '../../app/caches/shines_cache'
  require_relative '../../app/caches/tool_types_cache'
  require_relative '../../app/caches/tools_cache'

  CACHE_CLASSES = [
    PlanetsCache,
    LandsCache,
    PlotsCache,
    RaritiesCache,
    ShinesCache,
    ToolTypesCache,
    ToolsCache,
  ]

  desc "Clear caches"
  task clear: :environment do
    CACHE_CLASSES.each(&:clear)
  end

  desc "Write caches"
  task write: :environment do
    CACHE_CLASSES.each(&:write)
  end
end
