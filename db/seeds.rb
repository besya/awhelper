# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def seed_path(name)
  File.join(Rails.root, 'db', 'seeds', [name, 'rb'].join('.'))
end

load seed_path('planets')
load seed_path('rarities')
load seed_path('shines')
load seed_path('lands')
load seed_path('tool_types')
load seed_path('tools')
load seed_path('plots')
