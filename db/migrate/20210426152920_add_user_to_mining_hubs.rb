class AddUserToMiningHubs < ActiveRecord::Migration[6.1]
  def change
    add_belongs_to :mining_hubs, :user, after: :id
  end
end
