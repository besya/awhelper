class AddStackedToPlanets < ActiveRecord::Migration[6.1]
  def change
    add_column :planets, :stacked, :integer
  end
end
