class AddColumnsToTools < ActiveRecord::Migration[6.1]
  def change
    add_columns :tools, :original_id, :max_issuance, type: :string, after: :id
  end
end
