class AddUserToBuilds < ActiveRecord::Migration[6.1]
  def change
    add_belongs_to :builds, :user, after: :id
  end
end
