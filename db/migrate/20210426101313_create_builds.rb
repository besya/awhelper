class CreateBuilds < ActiveRecord::Migration[6.1]
  def change
    create_table :builds do |t|
      t.string :name
      t.references :tool_1
      t.references :tool_2
      t.references :tool_3
      t.timestamps
    end
  end
end
