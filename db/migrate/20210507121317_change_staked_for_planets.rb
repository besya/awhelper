class ChangeStakedForPlanets < ActiveRecord::Migration[6.1]
  def change
    rename_column :planets, :stacked, :staked
  end
end
