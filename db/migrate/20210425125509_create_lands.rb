class CreateLands < ActiveRecord::Migration[6.1]
  def change
    create_table :lands do |t|
      t.string :name

      t.decimal :charge_time,             precision: 15, scale: 10
      t.decimal :proof_of_work_reduction, precision: 15, scale: 10
      t.decimal :trilium_mining_power,    precision: 15, scale: 10
      t.decimal :nft_luck,                precision: 15, scale: 10

      t.timestamps
    end
  end
end
