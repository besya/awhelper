class AddTotalPoolToPlanets < ActiveRecord::Migration[6.1]
  def change
    add_column :planets, :total_pool, :integer, after: :id
  end
end
