class RemoveNameFromMiningHubs < ActiveRecord::Migration[6.1]
  def change
    remove_column :mining_hubs, :name, :string
  end
end
