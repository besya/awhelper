class RemoveToolsFromMiningHubs < ActiveRecord::Migration[6.1]
  def change
    remove_reference :mining_hubs, :tool_1
    remove_reference :mining_hubs, :tool_2
    remove_reference :mining_hubs, :tool_3
  end
end
