class AddCoordinatesToPlots < ActiveRecord::Migration[6.1]
  def change
    add_column :plots, :coordinate_x, :integer, after: :rarity_id
    add_column :plots, :coordinate_y, :integer, after: :coordinate_x
  end
end
