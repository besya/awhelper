class CreateTools < ActiveRecord::Migration[6.1]
  def change
    create_table :tools do |t|
      t.string :name

      t.belongs_to :rarity
      t.belongs_to :shine
      t.belongs_to :tool_type

      t.decimal :charge_time,             precision: 15, scale: 10
      t.decimal :proof_of_work_reduction, precision: 15, scale: 10
      t.decimal :trilium_mining_power,    precision: 15, scale: 10
      t.decimal :nft_luck,                precision: 15, scale: 10

      t.timestamps
    end
  end
end
