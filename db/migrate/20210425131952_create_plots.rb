class CreatePlots < ActiveRecord::Migration[6.1]
  def change
    create_table :plots do |t|
      t.belongs_to :planet
      t.belongs_to :land
      t.belongs_to :rarity

      t.decimal :owner_profit_share_from_mining, precision: 15, scale: 10
      t.string :land_owned_by

      t.timestamps
    end
  end
end
