class AddMintToPlots < ActiveRecord::Migration[6.1]
  def change
    add_column :plots, :mint, :integer, after: :land_owned_by
  end
end
