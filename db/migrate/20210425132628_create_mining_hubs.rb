class CreateMiningHubs < ActiveRecord::Migration[6.1]
  def change
    create_table :mining_hubs do |t|
      t.string :name
      t.references :plot
      t.references :tool_1
      t.references :tool_2
      t.references :tool_3
      t.timestamps
    end
  end
end
