class AddBuildToMiningHub < ActiveRecord::Migration[6.1]
  def change
    add_belongs_to :mining_hubs, :build, after: :plot_id
  end
end
