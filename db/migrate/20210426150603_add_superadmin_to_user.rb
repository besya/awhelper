class AddSuperadminToUser < ActiveRecord::Migration[6.1]
  def up
    add_column :users, :superadmin, :boolean, :null => false, :default => false

    User.create! do |r|
      r.email      = 'gravisbesya@list.ru'
      r.password   = '123456'
      r.superadmin = true
    end
  end

  def down
    remove_column :users, :superadmin
    User.find_by_email('gravisbesya@list.ru').try(:delete)
  end
end
