class AddMaxMineableToPlanets < ActiveRecord::Migration[6.1]
  def change
    add_column :planets, :max_mineable, :decimal,
               precision: 15, precision: 15, scale: 10
  end
end
