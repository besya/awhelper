class CreatePlanets < ActiveRecord::Migration[6.1]
  def change
    create_table :planets do |t|
      t.string :name
      t.decimal :current_mining_pot, precision: 15, scale: 10
      t.decimal :fillrate, precision: 15, scale: 10
      t.timestamps
    end
  end
end
