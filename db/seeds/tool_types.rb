TOOLS_TYPES = {
  extractor:   '',
  manipulator:   '',
  exotool:   '',
  explosive:   '',
}

TOOLS_TYPES.each_pair { |name, _| ToolType.create(name: name) }
