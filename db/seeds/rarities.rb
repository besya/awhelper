RARITIES = {
  abundant:   'grey',
  common:     'black',
  rare:       'blue',
  epic:       'violet',
  legendary:  'gold',
  mythical:   'red'
}

RARITIES.each_pair { |name, color| Rarity.create(name: name, color: color) }
