SHINES = {
  stone:   '',
  antimatter: '',
  gold: '',
  stardust: '',
}

SHINES.each_pair { |name, _| Shine.create(name: name) }
