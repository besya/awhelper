# https://docs.google.com/spreadsheets/d/1a9ecoafT2qbs4eXJ_3WTupq6j4itE7VG-lUTDTLX-Cc/edit#gid=1584521652

LANDS = "Sandy Coastline	1.5	0	1.3	1.8
Grassland	1	2	0.9	1
Rocky Crater	1.2	1	1	1.2
Grass Coastline	1.1	0	1	1
Icy Mountains	2	0	1.9	1.7
Inland River	1.5	0	1.4	1.3
Rocky Coastline	2	0	1.8	1.8
Mushroom Forest	2	0	1.4	2.2
Sandy Desert	1.2	2	1	1.1
Small Island	2.1	0	1.5	2.2
Rocky Desert	2	0	1.6	1.9
Dunes	1.5	1	1.3	1.3
Tree Forest	1.3	0	1.2	1
Geothermal Springs	1.5	0	1.6	0.8
Icy Desert	1.7	1	1.7	1
Plains	0.7	2	0.6	0.5
Mountains	3	0	2.2	2.2
Dormant Volcano	2.5	0	2.1	1.2
Methane Swampland	3.5	0	2.3	2.3
Active Volcano	5	0	2.5	2.5
".each_line.map do |line|
  line.chomp.split("\t")
end

  LANDS.each do |land|
    Land.create(
      name: land[0].delete(' ').underscore,
      charge_time: land[1],
      proof_of_work_reduction: land[2],
      trilium_mining_power: land[3],
      nft_luck: land[4]
    )
  end
