# https://metahub.info/2021/218-alien-world-tools-ultimate-guide/

# ID	Name	Rarity	Max Issuance	Shininess	Type	Charge time (sec)	POW	TLM	NFT luck	TLM efficiency	NFT efficiency	Total efficiency
TOOLS = "EQ320	Advanced TD	Epic	32	Antimatter	Manipulator	69	4	2.5	1	115.94%	86.96%	202.90%
EQ220	Advanced TD	Epic	128	Stardust	Manipulator	75	2	2.5	1	106.67%	80.00%	186.67%
EQ120	Advanced TD	Epic	512	Gold	Manipulator	80	2	2.5	0.5	100.00%	37.50%	137.50%
EQ020	Advanced TD	Epic	2048	Stone	Manipulator	80	2	2.5	0	100.00%	0.00%	100.00%
EQ328	AI Excavator	Mythical	2	Antimatter	Extractor	199	6	7	7	112.56%	211.06%	323.62%
EQ228	AI Excavator	Mythical	8	Stardust	Extractor	240	6	7	7	93.33%	175.00%	268.33%
EQ128	AI Excavator	Mythical	32	Gold	Extractor	270	6	6	6	71.11%	133.33%	204.44%
EQ028	AI Excavator	Mythical	128	Stone	Extractor	300	6	5	5	53.33%	100.00%	153.33%
EQ315	Artunian Shovel	Rare	64	Antimatter	Extractor	499	3	12	6	76.95%	72.14%	149.10%
EQ215	Artunian Shovel	Rare	256	Stardust	Extractor	600	2	10	6	53.33%	60.00%	113.33%
EQ115	Artunian Shovel	Rare	1024	Gold	Extractor	660	2	10	6	48.48%	54.55%	103.03%
EQ015	Artunian Shovel	Rare	4096	Stone	Extractor	750	2	10	5	42.67%	40.00%	82.67%
EQ402	Bananominer	Common	120	Stardust	Extractor	104	1	1.5	1	46.15%	57.69%	103.85%
EQ502	Bananominer	Common	480	Gold	Extractor	112	1	1.5	1	42.86%	53.57%	96.43%
EQ602	Bananominer	Common	1919	Stone	Extractor	120	1	1.5	1	40.00%	50.00%	90.00%
EQ317	Barrel Digger	Rare	64	Antimatter	Extractor	499	2	9	8	57.72%	96.19%	153.91%
EQ217	Barrel Digger	Rare	256	Stardust	Extractor	580	2	9	8	49.66%	82.76%	132.41%
EQ117	Barrel Digger	Rare	1024	Gold	Extractor	660	2	8	8	38.79%	72.73%	111.52%
EQ017	Barrel Digger	Rare	4096	Stone	Extractor	700	2	7	7	32.00%	60.00%	92.00%
EQ106	Basic Explosive	Common	4096	Gold	Explosive	1100	0	18	1	52.36%	5.45%	57.82%
EQ206	Basic Explosive	Common	1024	Stardust	Explosive	1100	0	18	1	52.36%	5.45%	57.82%
EQ006	Basic Explosive	Common	16384	Stone	Explosive	1200	0	18	1	48.00%	5.00%	53.00%
EQ208	Basic Trilium Detector	Common	1024	Stardust	Manipulator	140	1	4	0.5	91.43%	21.43%	112.86%
EQ108	Basic Trilium Detector	Common	4096	Gold	Manipulator	160	1	4	0.5	80.00%	18.75%	98.75%
EQ008	Basic Trilium Detector	Common	16384	Stone	Manipulator	170	1	4	0	75.29%	0.00%	75.29%
EQ318	Causian Attractor	Epic	32	Antimatter	Manipulator	499	2	0	12	0.00%	144.29%	144.29%
EQ218	Causian Attractor	Epic	128	Stardust	Manipulator	600	2	0	12	0.00%	120.00%	120.00%
EQ118	Causian Attractor	Epic	512	Gold	Manipulator	660	2	0	10	0.00%	90.91%	90.91%
EQ018	Causian Attractor	Epic	2048	Stone	Manipulator	660	2	0	10	0.00%	90.91%	90.91%
EQ030	Dacalizer	Epic	2048	Antimatter	Explosive	2899	0	38	16	41.95%	33.11%	75.06%
EQ030	Dacalizer	Epic	2048	Stardust	Explosive	3300	0	36	15	34.91%	27.27%	62.18%
EQ130	Dacalizer	Epic	2048	Gold	Explosive	3600	0	36	15	32.00%	25.00%	57.00%
EQ030	Dacalizer	Epic	2048	Stone	Explosive	4000	0	36	15	28.80%	22.50%	51.30%
EQ311	Draxos Axe	Rare	64	Antimatter	ExoTool	349	4	3	7	27.51%	120.34%	147.85%
EQ211	Draxos Axe	Rare	256	Stardust	ExoTool	400	3	3	7	24.00%	105.00%	129.00%
EQ111	Draxos Axe	Rare	1024	Gold	ExoTool	400	3	2	6	16.00%	90.00%	106.00%
EQ011	Draxos Axe	Rare	4096	Stone	ExoTool	420	3	1	6	7.62%	85.71%	93.33%
EQ324	Exlian Staff	Legendary	16	Antimatter	Exotool	499	3	11	6	70.54%	72.14%	142.69%
EQ224	Exlian Staff	Legendary	64	Stardust	Exotool	600	2	11	6	58.67%	60.00%	118.67%
EQ124	Exlian Staff	Legendary	256	Gold	Exotool	720	2	10	6	44.44%	50.00%	94.44%
EQ024	Exlian Staff	Legendary	1024	Stone	Exotool	720	2	10	6	44.44%	50.00%	94.44%
EQ209	ExoGloves	Common	1024	Stardust	ExoTool	32	2	1	0	100.00%	0.00%	100.00%
EQ109	ExoGloves	Common	4096	Gold	ExoTool	36	2	1	0	88.89%	0.00%	88.89%
EQ009	ExoGloves	Common	16384	Stone	ExoTool	40	2	1	0	80.00%	0.00%	80.00%
EQ204	Gasrigged Extractor	Common	1024	Stardust	Extractor	420	0	9	2	68.57%	28.57%	97.14%
EQ104	Gasrigged Extractor	Common	4096	Gold	Extractor	500	0	9	2	57.60%	24.00%	81.60%
EQ004	Gasrigged Extractor	Common	16384	Stone	Extractor	540	0	9	2	53.33%	22.22%	75.56%
EQ314	Glavor Disc	Rare	64	Antimatter	Manipulator	249	1	2	5	25.70%	120.48%	146.18%
EQ214	Glavor Disc	Rare	256	Stardust	Manipulator	260	1	1	4	12.31%	92.31%	104.62%
EQ114	Glavor Disc	Rare	1024	Gold	Manipulator	300	1	1	4	10.67%	80.00%	90.67%
EQ014	Glavor Disc	Rare	4096	Stone	Manipulator	300	1	1	4	10.67%	80.00%	90.67%
EQ205	Infused Extractor	Common	1024	Stardust	Extractor	330	1	8	1	77.58%	18.18%	95.76%
EQ105	Infused Extractor	Common	4096	Gold	Extractor	360	1	8	1	71.11%	16.67%	87.78%
EQ005	Infused Extractor	Common	16384	Stone	Extractor	360	1	8	0	71.11%	0.00%	71.11%
EQ312	Large Capacitor	Rare	64	Antimatter	Manipulator	1111	1	22	4	63.37%	21.60%	84.97%
EQ212	Large Capacitor	Rare	256	Stardust	Manipulator	1350	0	22	4	52.15%	17.78%	69.93%
EQ112	Large Capacitor	Rare	1024	Gold	Manipulator	1440	0	21	4	46.67%	16.67%	63.33%
EQ012	Large Capacitor	Rare	4096	Stone	Manipulator	1440	0	20	3	44.44%	12.50%	56.94%
EQ316	Large Explosive	Rare	64	Antimatter	Explosive	2999	0	32	11	34.14%	22.01%	56.15%
EQ116	Large Explosive	Rare	1024	Gold	Explosive	3300	0	30	10	29.09%	18.18%	47.27%
EQ216	Large Explosive	Rare	256	Stardust	Explosive	3300	0	30	10	29.09%	18.18%	47.27%
EQ016	Large Explosive	Rare	4096	Stone	Explosive	3600	0	30	10	26.67%	16.67%	43.33%
EQ322	Localised Attractor	Epic	32	Antimatter	Manipulator	399	3	8	6	64.16%	90.23%	154.39%
EQ122	Localised Attractor	Epic	512	Gold	Manipulator	450	1	7	6	49.78%	80.00%	129.78%
EQ222	Localised Attractor	Epic	128	Stardust	Manipulator	450	1	7	6	49.78%	80.00%	129.78%
EQ022	Localised Attractor	Epic	2048	Stone	Manipulator	450	1	6	5	42.67%	66.67%	109.33%
EQ327	Lucky Drill	Legendary	16	Antimatter	Extractor	133	2	5	2	120.30%	90.23%	210.53%
EQ227	Lucky Drill	Legendary	64	Stardust	Extractor	150	2	5	2	106.67%	80.00%	186.67%
EQ127	Lucky Drill	Legendary	256	Gold	Extractor	150	0	5	1.5	106.67%	60.00%	166.67%
EQ027	Lucky Drill	Legendary	1024	Stone	Extractor	160	0	5	1	100.00%	37.50%	137.50%
EQ323	Nanominer	Epic	32	Antimatter	Exotool	99	4	4	1	129.29%	60.61%	189.90%
EQ223	Nanominer	Epic	128	Stardust	Exotool	120	4	4	1	106.67%	50.00%	156.67%
EQ123	Nanominer	Epic	512	Gold	Exotool	140	3	4	1	91.43%	42.86%	134.29%
EQ023	Nanominer	Epic	2048	Stone	Exotool	150	3	4	1	85.33%	40.00%	125.33%
EQ026	Particle Beam Collider	Legendary	1024	Stone	Manipulator	1300	4	15	8	36.92%	36.92%	73.85%
EQ326	Particle Beam Extractor	Legendary	16	Antimatter	Manipulator	999	4	16	8	51.25%	48.05%	99.30%
EQ126	Particle Beam Extractor	Legendary	256	Gold	Manipulator	1200	4	16	8	42.67%	40.00%	82.67%
EQ226	Particle Beam Extractor	Legendary	64	Stardust	Manipulator	1200	4	16	8	42.67%	40.00%	82.67%
EQ203	Power Extractor	Common	1024	Stardust	Extractor	220	1	5	1	72.73%	27.27%	100.00%
EQ103	Power Extractor	Common	4096	Gold	Extractor	250	1	5	1	64.00%	24.00%	88.00%
EQ003	Power Extractor	Common	16384	Stone	Extractor	270	1	5	1	59.26%	22.22%	81.48%
EQ210	Power Saw 	Common	1024	Stardust	Manipulator	330	1	7	2.2	67.88%	40.00%	107.88%
EQ110	Power Saw 	Common	4096	Gold	Manipulator	360	1	7	2.2	62.22%	36.67%	98.89%
EQ010	Power Saw 	Common	16384	Stone	Manipulator	360	1	6	2	53.33%	33.33%	86.67%
EQ313	Processing Ring	Rare	64	Antimatter	Manipulator	349	3	1	8	9.17%	137.54%	146.70%
EQ213	Processing Ring	Rare	256	Stardust	Manipulator	440	1	1	8	7.27%	109.09%	116.36%
EQ113	Processing Ring	Rare	1024	Gold	Manipulator	500	1	1	8	6.40%	96.00%	102.40%
EQ013	Processing Ring	Rare	4096	Stone	Manipulator	600	1	1	8	5.33%	80.00%	85.33%
EQ325	Quantum Drill	Legendary	16	Antimatter	Extractor	2999	4	40	10	42.68%	20.01%	62.69%
EQ225	Quantum Drill	Legendary	64	Stardust	Extractor	3600	4	40	10	35.56%	16.67%	52.22%
EQ125	Quantum Drill	Legendary	256	Gold	Extractor	4200	4	40	5	30.48%	7.14%	37.62%
EQ025	Quantum Drill	Legendary	1024	Stone	Extractor	4500	4	40	5	28.44%	6.67%	35.11%
EQ319	Quark Separator	Epic	32	Antimatter	Explosive	2999	1	35	15	37.35%	30.01%	67.36%
EQ219	Quark Separator	Epic	128	Stardust	Explosive	3600	0	35	14	31.11%	23.33%	54.44%
EQ119	Quark Separator	Epic	512	Gold	Explosive	4300	0	35	14	26.05%	19.53%	45.58%
EQ019	Quark Separator	Epic	2048	Stone	Explosive	4500	0	35	12	24.89%	16.00%	40.89%
EQ321	RD9000 Excavator	Epic	32	Antimatter	Extractor	199	2	7	1	112.56%	30.15%	142.71%
EQ221	RD9000 Excavator	Epic	128	Stardust	Extractor	220	2	7	1	101.82%	27.27%	129.09%
EQ121	RD9000 Excavator	Epic	512	Gold	Extractor	240	2	7	1	93.33%	25.00%	118.33%
EQ021	RD9000 Excavator	Epic	2048	Stone	Extractor	240	2	7	0	93.33%	0.00%	93.33%
EQ107	Standard Capacitor	Abundant	Unlimited	Gold	Manipulator	70	1	1	0.5	45.71%	42.86%	88.57%
EQ007	Standard Capacitor	Abundant	Unlimited	Stone	Manipulator	75	1	1	0.5	42.67%	40.00%	82.67%
EQ102	Standard Drill	Abundant	Unlimited	Gold	Extractor	115	1	2	0.7	55.65%	36.52%	92.17%
EQ002	Standard Drill	Abundant	Unlimited	Stone	Extractor	120	1	2	0.7	53.33%	35.00%	88.33%
EQ101	Standard Shovel	Abundant	Unlimited	Gold	Extractor	75	0	1	0.5	42.67%	40.00%	82.67%
EQ001	Standard Shovel	Abundant	Unlimited	Stone	Extractor	80	0	1	0.5	40.00%	37.50%	77.50%
EQ329	Waxtural Processor	Mythical	2	Antimatter	Manipulator	1499	3	25	28	53.37%	112.07%	165.44%
EQ229	Waxtural Processor	Mythical	8	Stardust	Manipulator	1750	1	25	28	45.71%	96.00%	141.71%
EQ129	Waxtural Processor	Mythical	32	Gold	Manipulator	2100	1	25	28	38.10%	80.00%	118.10%
EQ029	Waxtural Processor	Mythical	128	Stone	Manipulator	2400	1	20	25	26.67%	62.50%	89.17%
".each_line.map do |line|
  line.chomp.split("\t")
end

TOOLS.each do |tool|
  Tool.create(
    original_id: tool[0],
    name: tool[1],
    rarity: Rarity.find_by(name: tool[2].downcase.to_sym),
    max_issuance: tool[3],
    shine: Shine.find_by(name: tool[4].downcase),
    tool_type: ToolType.find_by(name: tool[5].downcase),
    charge_time: tool[6],
    proof_of_work_reduction: tool[7],
    trilium_mining_power: tool[8],
    nft_luck: tool[9]
  )
end
