PLANETS = {
  eyeke:  { pot: 1.5853, fillrate: 0.9392318 },
  kavian: { pot: 2.0533, fillrate: 1.916077 },
  magor:  { pot: 1.5655, fillrate: 0.8546854 },
  naron:  { pot: 0.7154, fillrate: 0.9320104 },
  neri:   { pot: 0.3663, fillrate: 2.772939 },
  veles:  { pot: 0.7784, fillrate: 0.7742289 },
}

PLANETS.each_pair do |name, options|
  Planet.create(name: name, current_mining_pot: options[:pot], fillrate: options[:fillrate])
end

FetchPlanetsJob.perform_now
