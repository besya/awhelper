# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_07_191817) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource"
  end

  create_table "builds", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.bigint "tool_1_id"
    t.bigint "tool_2_id"
    t.bigint "tool_3_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["tool_1_id"], name: "index_builds_on_tool_1_id"
    t.index ["tool_2_id"], name: "index_builds_on_tool_2_id"
    t.index ["tool_3_id"], name: "index_builds_on_tool_3_id"
    t.index ["user_id"], name: "index_builds_on_user_id"
  end

  create_table "lands", force: :cascade do |t|
    t.string "name"
    t.decimal "charge_time", precision: 15, scale: 10
    t.decimal "proof_of_work_reduction", precision: 15, scale: 10
    t.decimal "trilium_mining_power", precision: 15, scale: 10
    t.decimal "nft_luck", precision: 15, scale: 10
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mining_hubs", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "plot_id"
    t.bigint "build_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["build_id"], name: "index_mining_hubs_on_build_id"
    t.index ["plot_id"], name: "index_mining_hubs_on_plot_id"
    t.index ["user_id"], name: "index_mining_hubs_on_user_id"
  end

  create_table "planets", force: :cascade do |t|
    t.string "name"
    t.decimal "current_mining_pot", precision: 15, scale: 10
    t.decimal "fillrate", precision: 15, scale: 10
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "staked"
    t.integer "total_pool"
    t.decimal "max_mineable", precision: 15, scale: 10
  end

  create_table "plots", force: :cascade do |t|
    t.bigint "planet_id"
    t.bigint "land_id"
    t.bigint "rarity_id"
    t.integer "coordinate_x"
    t.integer "coordinate_y"
    t.decimal "owner_profit_share_from_mining", precision: 15, scale: 10
    t.string "land_owned_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "mint"
    t.index ["land_id"], name: "index_plots_on_land_id"
    t.index ["planet_id"], name: "index_plots_on_planet_id"
    t.index ["rarity_id"], name: "index_plots_on_rarity_id"
  end

  create_table "rarities", force: :cascade do |t|
    t.string "name"
    t.string "color"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "shines", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tool_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tools", force: :cascade do |t|
    t.string "max_issuance"
    t.string "original_id"
    t.string "name"
    t.bigint "rarity_id"
    t.bigint "shine_id"
    t.bigint "tool_type_id"
    t.decimal "charge_time", precision: 15, scale: 10
    t.decimal "proof_of_work_reduction", precision: 15, scale: 10
    t.decimal "trilium_mining_power", precision: 15, scale: 10
    t.decimal "nft_luck", precision: 15, scale: 10
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["rarity_id"], name: "index_tools_on_rarity_id"
    t.index ["shine_id"], name: "index_tools_on_shine_id"
    t.index ["tool_type_id"], name: "index_tools_on_tool_type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "superadmin", default: false, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
