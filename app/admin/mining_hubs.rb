ActiveAdmin.register MiningHub do
  menu priority: 2
  config.sort_order = 'id_asc'

  preserve_default_filters!
  remove_filter :plot
  # remove_filter :tool_1

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :plot_id, :build_id, :user_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :plot_id, :tool_1_id, :tool_2_id, :tool_3_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  def row_class(record)
    # tools = record.tools.map.with_index { |t, i| "tool_#{i+1}_#{t.rarity.name}" }.join(" ")
    # "plot_#{record.plot.rarity.name} #{tools}"
  end

  index(row_class: ->(record) { row_class(record) } ) do
  # index do
    selectable_column
    id_column
    column :user
    column :plot
    column :build
    actions
  end

  form do |f|
    f.semantic_errors # shows errors on :base
    f.inputs do         # builds an input field for every attribute
      # f.input :plot
      # f.input :plot_id, as: :search_select, url: admin_plots_path,
      #         fields: [:id, 'land.name'], display_name: 'display_name', minimum_input_length: 2,
      #         order_by: 'id_asc'
      f.input :user
      f.input :plot_id, as: :nested_select,
              level_1: {
                attribute: :planet_id,
                collection: Planet.all,
                display_name: :display_name,
                order_by: 'id_asc',
                minimum_input_length: 0,
              },
              level_2: {
                order_by: 'id_asc',
                attribute: :plot_id,
                minimum_input_length: 0,
                fields: [:land_name, :id],
                display_name: :name,
              }
      f.input :build
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end

  controller do
    def scoped_collection
      end_of_association_chain.includes([:plot, :build])
    end
  end
end
