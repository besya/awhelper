ActiveAdmin.register Planet do
  menu priority: 5
  menu parent: "Info"
  actions :index, :show

  config.sort_order = 'id_asc'

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :current_mining_pot, :fillrate
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :current_mining_pot, :fillrate]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index do
    # index do
    selectable_column
    id_column
    column :name, :display_name
    column :current_mining_pot
    column :fillrate
    actions
  end
end
