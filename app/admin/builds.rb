ActiveAdmin.register Build do
  menu priority: 3
  config.sort_order = 'id_asc'

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :tool_1_id, :tool_2_id, :tool_3_id, :user_id
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :tool_1_id, :tool_2_id, :tool_3_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  def row_class(record)
    "#{record.tools.map.with_index { |t, i| "tool_#{i+1}_#{t.rarity.name}" }.join(" ")}"
  end

  index(row_class: ->(record) { row_class(record) } ) do
    # index do
    selectable_column
    id_column
    column :user
    column :name, :display_name
    column :tool_1
    column :tool_2
    column :tool_3
    actions
  end
end
