ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }


  content title: proc { I18n.t("active_admin.dashboard") } do
    # div class: "blank_slate_container", id: "dashboard_default_message" do
    #   span class: "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "Mining Hubs" do
          table_for MiningHub.full_includes.where(user: current_user).order(:id).recent(10), sortable: true do
            column(:id)
            column(:plot)
            column(:owner, sortable: false) { |hub| hub.plot.land_owned_by }
            column(:build)
            column(:commission) { |hub| "#{hub.mine.commission}%" }
            column(:trilium_per_minute) { |hub| hub.mine.trilium_per_minute }
            column(:trilium_per_mine) { |hub| hub.mine.trilium_per_mine }
            column(:charge_time) { |hub| hub.mine.charge_time.to_i.pretty_duration }
            column(:nft_luck) { |hub| "#{hub.mine.nft_luck}%" }
            column(:trilium_mining_power) { |hub| "#{hub.mine.trilium_mining_power}%" }
            column(:effectiveness) { |hub| "#{hub.mine.effectiveness}%" }
            column(:timer) do |hub|
              div class: "timer", "data-hub-id": hub.id do
                span class: "days countdown-time"
                span class: "hours countdown-time"
                span class: "minutes countdown-time"
                span class: "seconds countdown-time"
              end
            end
            column do |hub|
              a "Start", href: "#", class: "timer-button", "data-hub-id": hub.id,"data-charge-time": hub.mine.charge_time
              link_to "Delete", admin_dashboard_delete_hub_path(hub_id: hub.id), method: :delete
            end
          end
        end
      end
    end

    columns do
      column do
        panel "Recommendations" do
          render "select_build"
          table_for Recommendations.new(
            build: Build.find_by(id: params[:build_id]) || Build.where(user_id: current_user.id).with_tools.last,
            order_field: params[:order]&.sub(/_desc|_asc/, '').presence || :trilium_per_mine,
            order_direction: params[:order]&.sub(/.*(desc|asc)/, '\1').presence || :desc,
            limit: 10
          ).fetch, sortable: true do
            column(:plot, sortable: false)
            column(:owner, sortable: false) { |mine| mine.plot.land_owned_by }
            # column(:build, sortable: false)
            column(:commission) { |mine| "#{mine.commission}%" }
            column(:trilium_per_minute)
            column(:trilium_per_mine)
            column(:charge_time) { |mine| mine.charge_time.to_i.pretty_duration }
            column(:nft_luck) { |mine| "#{mine.nft_luck}%" }
            column(:trilium_mining_power) { |mine| "#{mine.trilium_mining_power}%" }
            column(:effectiveness) { |mine| "#{mine.effectiveness}%" }
            column do |mine|
              link_to "Follow", admin_dashboard_create_hub_path(build_id: mine.build.id, plot_id: mine.plot.id), method: :post
            end
          end
        end
      end
    end
  end # content

  page_action :create_hub, method: :post do
    MiningHub.create(plot_id: params[:plot_id], build_id: params[:build_id], user: current_user)
    redirect_to admin_dashboard_path
  end

  page_action :delete_hub, method: :delete do
    MiningHub.destroy(params[:hub_id])
    redirect_to admin_dashboard_path
  end
end
