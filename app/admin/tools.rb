ActiveAdmin.register Tool do
  menu priority: 6
  menu parent: "Info"
  actions :index, :show

  config.sort_order = 'id_asc'

  # decorate_with RarityDecorator
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :rarity_id, :shine_id, :tool_type_id, :charge_time, :proof_of_work_reduction, :trilium_mining_power, :nft_luck
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :rarity_id, :shine_id, :tool_type_id, :charge_time, :proof_of_work_reduction, :trilium_mining_power, :nft_luck]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  #

  index(row_class: ->(record) { record.rarity.name } ) do
    selectable_column
    id_column
    column :name
    column :rarity
    column :shine
    column :tool_type
    column :charge_time
    column :proof_of_work_reduction
    column :trilium_mining_power
    column :nft_luck
    actions
  end
end
