ActiveAdmin.register Plot do
  menu priority: 3
  config.sort_order = 'id_asc'
  menu parent: "Info"
  actions :index, :show


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :planet_id, :land_id, :rarity_id, :coordinate_x, :coordinate_y,
                :owner_profit_share_from_mining, :land_owned_by
  #
  # or
  #
  # permit_params do
  #   permitted = [:planet_id, :land_id, :rarity_id, :owner_profit_share_from_mining, :land_owned_by]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  index(row_class: ->(record) { record.rarity.name } ) do
    selectable_column
    id_column
    column :planet, sortable: 'planet.id'
    column :land, sortable: 'land.id'
    column :rarity, sortable: 'rarity.id'
    column :coordinate_x
    column :coordinate_y
    column :owner_profit_share_from_mining
    column :land_owned_by
    # column :created_at
    # column :updated_at
    actions
  end

  controller do
    def scoped_collection
      end_of_association_chain.includes([:land, :planet, :rarity])
    end
  end
end
