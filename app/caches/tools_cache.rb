class ToolsCache
  include ModelCache

  key :tools
  fetcher -> { Tool.includes(:rarity, :shine, :tool_type).all.to_a }
end
