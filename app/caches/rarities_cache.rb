class RaritiesCache
  include ModelCache

  key :rarities
  fetcher -> { Rarity.all.to_a }
end
