class ToolTypesCache
  include ModelCache

  key :tool_types
  fetcher -> { ToolType.all.to_a }
end
