class PlotsCache
  include ModelCache

  key :plots
  fetcher -> { Plot.includes(:planet, :land, :rarity).all.to_a }
end
