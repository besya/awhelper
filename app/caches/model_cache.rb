module ModelCache
  extend ActiveSupport::Concern

  included do
    class_attribute :_cache_key
    class_attribute :_fetcher
  end

  class_methods do ||
    def key(_key_name)
      self._cache_key = _key_name
    end

    def fetcher(block)
      self._fetcher = block
    end

    def read
      Rails.cache.read(self._cache_key)
    end

    def clear
      Rails.cache.delete(self._cache_key)
    end

    def write
      Rails.cache.write(self._cache_key, data)
    end

    def fetch
      Rails.cache.fetch(self._cache_key) do
        data
      end
    end

    def data
      ActiveModelSerializers::SerializableResource.new(self._fetcher.call).to_json
    end
  end
end
