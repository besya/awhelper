class PlanetsCache
  include ModelCache

  key :planets
  fetcher -> { Planet.all.to_a }
end
