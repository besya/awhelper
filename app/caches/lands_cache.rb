class LandsCache
  include ModelCache

  key :lands
  fetcher -> { Land.all.to_a }
end
