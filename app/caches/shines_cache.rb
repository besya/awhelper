class ShinesCache
  include ModelCache

  key :shines
  fetcher -> { Shine.all.to_a }
end
