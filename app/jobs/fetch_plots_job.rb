class FetchPlotsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    land_stats = AwstatsLands.new.lands

    planets = Planet.all
    rarities = Rarity.all
    lands = Land.all

    land_stats.each do |land_stat|
      planet_name, rarity_name, mint, description, charge, mining, pow, nft, commission, x, y, user, id = land_stat
      land_name = description.split(' on ').first.delete(' ').underscore
      land = lands.find { |l| l.name == land_name }
      planet = planets.find { |p| p.name == planet_name.underscore }
      rarity = rarities.find { |r| r.name ==  rarity_name.underscore}

      plot = Plot.where(planet: planet, coordinate_x: x, coordinate_y: y).first_or_initialize
      plot.attributes = {
        planet: planet,
        land:   land,
        rarity: rarity,
        coordinate_x: x,
        coordinate_y: y,
        owner_profit_share_from_mining: commission,
        land_owned_by: user
      }
      plot.save
    end

    PlotsCache.write

    true
  rescue => e
    p e.message
  end
end
