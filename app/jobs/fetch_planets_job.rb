class FetchPlanetsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # mining_pools = AwstatsPools.new.mining_pools
    #
    # mining_pools.each do |pool|
    #   planet_name, _, mining_pool, _, fillrate = pool
    #
    #   planet = Planet.where(name: planet_name.underscore).first_or_initialize
    #   planet.attributes = {
    #     current_mining_pot: mining_pool.to_f,
    #     fillrate: fillrate.to_f
    #   }
    #   planet.save
    # end

    stats = WaxStats.new.get
    p "Planet stats received: #{stats}"

    stats.each do |stat|
      Planet.where(name: stat[:planet]).update_all(current_mining_pot: stat[:pot].to_f, stacked: stat[:stacked].to_i)
    end

    p "Planet stats were updated"

    PlanetsCache.write

    true
  end
end
