class MiningHub < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :plot, optional: true
  belongs_to :build, optional: true

  scope :full_includes, -> () { includes(:user, plot: [:planet, :land], build: [:tool_1, :tool_2, :tool_3]) }
  scope :recent, -> (limit) { order(updated_at: :desc).limit(limit) }
  scope :by_user, -> (user) { where(user: user) }

  def mine
    @mine ||= Mine.new(build: build, plot: plot)
  end
end
