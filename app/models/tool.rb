class Tool < ApplicationRecord
  belongs_to :rarity
  belongs_to :shine
  belongs_to :tool_type

  def display_name
    "#{name.titleize} #{shine.display_name}/#{rarity.display_name}"
  end

  def short_description
    stats.map { |k,v| [k,v].join(':') }.join(', ')
  end

  def stats
    {
      ct: charge_time,
      pow: proof_of_work_reduction,
      tlm: trilium_mining_power,
      nft: nft_luck
    }
  end

  def tlm_efficiency
    (trilium_mining_power / charge_time * 100 / 3.125 * 100).to_f.round(2)
  end

  def nft_efficiency
    (nft_luck / charge_time * 100 / (5.0 / 300 * 100) * 100).to_f.round(2)
  end

  def total_efficiency
    (tlm_efficiency.to_f + nft_efficiency.to_f).round(2)
  end
end
