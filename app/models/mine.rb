class Mine
  include ActiveModel::Serialization

  # @return [Build]
  attr_accessor :build

  # @return [Plot]
  attr_accessor :plot

  def initialize(build:, plot:)
    @build = build
    @plot = plot
  end

  # @return [Land]
  def land
    plot.land
  end

  # @return [Planet]
  def planet
    plot.planet
  end

  def commission
    plot.owner_profit_share_from_mining
  end

  def trilium_mining_power
    land.trilium_mining_power * build.trilium_mining_power -
      land.trilium_mining_power * build.trilium_mining_power * (commission / 100)
  end

  def nft_luck
    land.nft_luck * build.nft_luck
  end

  def trilium_per_mine
    trilium_mining_power * planet.current_mining_pot / 100
  end

  def charge_time
    land.charge_time * build.charge_time
  end

  def effectiveness
    trilium_mining_power + nft_luck
  end

  def trilium_per_minute
    (trilium_per_mine/charge_time * 60).round(8)
  end
end
