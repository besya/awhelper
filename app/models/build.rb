class Build < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :tool_1, class_name: Tool.to_s, optional: true
  belongs_to :tool_2, class_name: Tool.to_s, optional: true
  belongs_to :tool_3, class_name: Tool.to_s, optional: true

  scope :with_tools, -> () { includes(:tool_1, :tool_2, :tool_3) }
  scope :by_user, -> (user) { where(user: user) }

  def tools
    @tools ||= [tool_1, tool_2, tool_3].compact
  end

  def trilium_mining_power
    tools.map(&:trilium_mining_power).sum
  end

  def nft_luck
    tools.map(&:nft_luck).sum
  end

  def charge_time
    tools.map(&:charge_time).max(2).sum
  end

  def display_name
    name.presence || tools.map(&:short_description).join(" | ")
  end
end
