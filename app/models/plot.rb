class Plot < ApplicationRecord
  belongs_to :planet
  belongs_to :land
  belongs_to :rarity

  def display_name
    "#{land.display_name} on #{planet.display_name} [#{coordinate_x}:#{coordinate_y}]"
  end

  def coordinates
    "#{coordinate_x}:#{coordinate_y}"
  end

  def name
    "#{land.display_name} [#{coordinates}] [Commission: #{owner_profit_share_from_mining}% | #{land.short_description}]"
  end

  def as_json(options = nil)
    super(options).merge(
      {
        coordinates: coordinates,
        land_name: land.display_name,
        land_name_with_coords: "#{land.display_name} [#{coordinates}]",
        display_name: display_name,
        name: name,
      }
    )
  end
end
