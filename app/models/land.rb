class Land < ApplicationRecord
  def display_name
    name.titleize
  end

  def short_description
    "Charge Time: #{charge_time} | POW: #{proof_of_work_reduction} | TLM: #{trilium_mining_power} | NFT: #{nft_luck}"
  end
end
