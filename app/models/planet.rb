class Planet < ApplicationRecord
  def display_name
    name.titleize
  end

  def as_json(options = nil)
    super(options).merge(
      {
        display_name: display_name
      }
    )
  end

  def percent
    (staked.to_f / self.class.sum(:staked)) * 100
  end
end
