module InRarity
  def in_rarity(text, color)
    "<div style='text-align:center; padding: 5px 3px; color: white; background-color: #{color}'>#{text}</div>".html_safe
  end
end
