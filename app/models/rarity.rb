class Rarity < ApplicationRecord
  def display_name
    name.titleize
  end
end
