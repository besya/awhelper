class LandSerializer < ActiveModel::Serializer
  attributes :id

  attribute :display_name, key: :name

  attribute :charge_time do
    object.charge_time.to_f
  end

  attribute :proof_of_work_reduction do
    object.proof_of_work_reduction.to_f
  end

  attribute :trilium_mining_power do
    object.trilium_mining_power.to_f
  end

  attribute :nft_luck do
    object.nft_luck.to_f
  end
end
