class PlanetSerializer < ActiveModel::Serializer
  attributes :id, :total_pool
  attribute :display_name, key: :name

  attribute :current_mining_pot do
    object.current_mining_pot.to_f
  end

  attribute :fillrate do
    object.fillrate.to_f
  end

  attribute :staked do
    object.staked.to_f
  end

  attribute :max_mineable do
    object.max_mineable.to_f
  end

  attribute :percent do
    object.percent.to_f.round(1)
  end
end
