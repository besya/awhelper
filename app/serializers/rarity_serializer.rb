class RaritySerializer < ActiveModel::Serializer
  attributes :id, :name
end
