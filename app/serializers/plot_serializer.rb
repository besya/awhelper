class PlotSerializer < ActiveModel::Serializer
  attributes :id, :planet_id, :land_id, :rarity_id, :coordinate_x, :coordinate_y, :land_owned_by, :mint

  attribute :planet_name do
    object.planet.display_name
  end

  attribute :land_name do
    object.land.display_name
  end

  attribute :rarity do
    object.rarity.display_name
  end

  attribute :owner_profit_share_from_mining do
    object.owner_profit_share_from_mining.to_f
  end
end
