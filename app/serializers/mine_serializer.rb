class MineSerializer < ActiveModel::Serializer
  attributes :plot, :build, :commission, :trilium_mining_power, :nft_luck, :trilium_per_mine, :charge_time,
             :effectiveness, :trilium_per_minute
end
