class ToolTypeSerializer < ActiveModel::Serializer
  attributes :id, :name
end
