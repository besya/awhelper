class MiningHubSerializer < ActiveModel::Serializer
  attributes :id

  attribute :mine do
    MineSerializer.new(object.mine)
  end
end
