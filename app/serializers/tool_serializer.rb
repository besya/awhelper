class ToolSerializer < ActiveModel::Serializer
  attributes :id, :original_id, :name, :max_issuance, :rarity_id, :shine_id, :tool_type_id

  attribute :charge_time do
    object.charge_time.to_f
  end

  attribute :proof_of_work_reduction do
    object.proof_of_work_reduction.to_f
  end

  attribute :trilium_mining_power do
    object.trilium_mining_power.to_f
  end

  attribute :nft_luck do
    object.nft_luck.to_f
  end

  attribute :tlm_efficiency do
    object.tlm_efficiency.to_f
  end

  attribute :nft_efficiency do
    object.nft_efficiency.to_f
  end

  attribute :total_efficiency do
    object.total_efficiency.to_f
  end

  attribute :rarity do
    object.rarity.display_name
  end

  attribute :shine do
    object.shine.display_name
  end

  attribute :type do
    object.tool_type.display_name
  end
end
