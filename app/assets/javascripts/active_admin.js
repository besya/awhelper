//= require active_admin/base
//= require activeadmin_addons/all

window.setTimeout(function () {
  const path = window.location.pathname;

  if (path == "/admin" || path == "/admin/dashboard" || path == "/admin/planets") {
    window.location.reload();
  }
}, 10000);
//
// window.document.onload(function() {
//   console.log('loaded');
// });

function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}
function initializeClock(clock, endtime) {
  var clock = $(clock);
  var daysSpan = clock.find('.days');
  var hoursSpan = clock.find('.hours');
  var minutesSpan = clock.find('.minutes');
  var secondsSpan = clock.find('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);
    hoursSpan.text(`${('0' + t.hours).slice(-2)}:`);
    minutesSpan.text(`${('0' + t.minutes).slice(-2)}:`);
    secondsSpan.text(('0' + t.seconds).slice(-2));

    if (t.total <= 0) {
      clearInterval(timeinterval);
      daysSpan.text("");
      hoursSpan.text("");
      minutesSpan.text("");
      secondsSpan.text("");
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

$( document ).ready(function(){
  for(let key in localStorage) {
    if (!localStorage.hasOwnProperty(key)) {
      continue; // пропустит такие ключи, как "setItem", "getItem" и так далее
    }

    initializeClock(`.timer[data-hub-id="${key}"]`, localStorage.getItem(key));
  }

  $('.timer-button').click(function(){
    const chargeTime = parseInt($(this).attr("data-charge-time"));
    const hubId = $(this).attr("data-hub-id");
    const deadline = new Date(new Date().getTime()+chargeTime*1000);
    localStorage.setItem(hubId, deadline);
    initializeClock(`.timer[data-hub-id="${hubId}"]`, deadline);
  });
});
