import React from "react"
import { slice, selectors } from "../../features/tracksTable"
import { TablePage } from "../tablePage/TablePage"

export const Tracks = ({visible}) => {
  if(!visible) { return null }

  return(
    <TablePage
      tableName="tracksTable"
      title="Track Your Mining Hubs"
      selectors={selectors}
      actions={slice.actions}
    />
  )
}
