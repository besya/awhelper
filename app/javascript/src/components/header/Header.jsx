import React from "react"
import { routes } from "../../router/routes"
import { NavLink } from "react-router-dom"
import { Container, Loader, Menu, Transition } from "semantic-ui-react"
import { useSelector } from "react-redux"
import { selectors } from "../../features/global"

const Loading = () => {
  const loading = useSelector(selectors.selectLoading)

  return(
    <Transition visible={loading} animation='fade' duration={1000}>
      <div style={{position: 'fixed', right: '25px', top: '25px', fontSize: '20px', width: '25px', height: '25px', zIndex: '5'}}>
        <Loader active={true} inline size="small"/>
      </div>
    </Transition>
  )
}

export const Header = () => {
  return(
    <Menu size='large'>
      <Container fluid>
        {routes.map(({ title, path, exact }, i) => (
          <NavLink exact={exact} to={path} key={title} name={title} className="item">
            {title}
          </NavLink>
        ))}
      </Container>
      <Loading/>
    </Menu>
  )
}
