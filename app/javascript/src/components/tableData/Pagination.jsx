import { Pagination as PaginationUI } from "semantic-ui-react"

export const Pagination = ({page, totalPages, handlePageChange}) => {
  return(
    <PaginationUI
      floated="right"
      activePage={page}
      totalPages={totalPages}
      onPageChange={(e, { activePage }) => handlePageChange(activePage)}
      ellipsisItem={undefined}
      firstItem={undefined}
      lastItem={undefined}
    />
  )
}
