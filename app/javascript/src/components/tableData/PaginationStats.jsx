export const PaginationStats = ({offset, limit, count, currentCount}) => {
  let text

  if (count < 1) {
    text = `Nothing to display`
  } else if (offset + 1 == count) {
    text = `Showing 1 item`
  } else if (limit > currentCount){
    text = `Showing ${offset + 1}-${offset + currentCount} from ${count} items`
  } else {
    text = `Showing ${offset + 1}-${offset + limit} from ${count} items`
  }

  return(
    <div style={{ fontSize: '12px', lineHeight: 'normal' }}>
      <i>{text}</i>
    </div>
  )
}
