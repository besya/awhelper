import React from "react"
import { Dropdown } from "semantic-ui-react"

export const PerPage = ({
  current,
  handlePerPageChange
}) => {
  return(
    <Dropdown
      compact
      selection
      onChange={(e, { value }) => handlePerPageChange(value)}
      defaultValue={current}
      options={[{key: 5, text: 5, value: 5}, {key: 15, text: 15, value: 15}, {key: 30, text: 30, value: 30}, {key: 50, text: 50, value: 50}]}
      style={{
        float: 'right',
        paddingTop: '.92857143em',
        paddingBottom: '.92857143em',
        boxShadow: '0 1px 2px 0 rgb(34 36 38 / 15%)'
      }}
    />
  )
}
