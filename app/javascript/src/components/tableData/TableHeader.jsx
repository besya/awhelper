import { Table as TableUI } from "semantic-ui-react"
import { Filter } from "./Filter"
import React from "react"

export const TableHeader = ({
  columns,
  column,
  direction,
  filter,
  filterOptions,
  changeSort,
  handleFilterChange
}) => (
  <TableUI.Header>
    <TableUI.Row>
      {columns.map(({ name, title, width, type }, i) => (
        <TableUI.HeaderCell
          key={`${name}-sort-cell-${i}`}
          sorted={column === name ? direction : null}
          onClick={() => type != 'action' ? changeSort(name) : null }
          width={width}
        >
          {title}
        </TableUI.HeaderCell>
      ))}
    </TableUI.Row>
    <TableUI.Row>
      {columns.map(({ name, title, type }, i) => (
        <TableUI.Cell style={{overflow: 'initial'}} key={`${name}-filter-cell-${i}`}>
          <Filter
            name={name}
            title={title}
            type={type}
            value={filter[name] ? filter[name].value : ''}
            onChange={handleFilterChange}
            options={filterOptions[name] ? filterOptions[name].options : []}
          />
        </TableUI.Cell>
      ))}
    </TableUI.Row>
  </TableUI.Header>
)
