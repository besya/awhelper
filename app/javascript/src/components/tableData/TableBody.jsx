import { Table as TableUI } from "semantic-ui-react"
import { TableRow } from "./TableRow"
import React from "react"

export const TableBody = ({
  data,
  columns
}) => (
  <TableUI.Body>
    {data.map((item, i) => (
      <TableRow key={`${name}-show-row-${i}`} item={item} columns={columns}/>
    ))}
  </TableUI.Body>
)
