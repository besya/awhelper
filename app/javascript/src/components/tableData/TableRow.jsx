import React  from "react"
import { Button, Table } from "semantic-ui-react"
import { useDispatch } from "react-redux"

const colors = {
  abundant: 'rgba(224,224,224,0.1)',
  common: 'rgba(155,155,155,0.1)',
  rare: 'rgba(102,175,223,0.1)',
  epic: 'rgba(198,136,179,0.1)',
  legendary: 'rgba(210,150,28,0.1)',
  mythical: 'rgba(201,64,64,0.1)',
  null: ''
}

export const TableRow = ({
  columns,
  item,
}) => {
  const { rarity } = item
  const color = colors[`${rarity}`.toLowerCase()]
  const dispatch = useDispatch()

  return(
    <Table.Row style={{backgroundColor: color}}>
      {columns.map(({ id, name, type, action, actionTitle }, i) => (
        <Table.Cell key={`${name}-show-cell-${i}`}>
          {(type == 'action') ?
            <Button fluid onClick={() => (dispatch({type: action, payload: item}))}>{actionTitle}</Button>
            :
            <div>{item[name]}</div>
          }
        </Table.Cell>
      ))}
    </Table.Row>
  )
}
