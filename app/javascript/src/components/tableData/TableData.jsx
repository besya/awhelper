import React from "react"
import { Grid, Header, Loader, Transition } from "semantic-ui-react"
import { PaginationStats } from "./PaginationStats"
import { Pagination } from "./Pagination"
import { PerPage } from "./PerPage"
import { Table } from "./Table"
import "../../../stylesheets/styles.css"

export const TableData = ({
  title,
  offset,
  limit,
  count,
  page,
  totalPages,
  columns,
  changeSort,
  column,
  direction,
  data,
  status,
  filter,
  filterOptions,
  handlePageChange,
  handlePerPageChange,
  handleFilterChange,
}) => {
  // console.log(`${title} Table data renders`)
  const currentCount = data.length
  const loading = status == "loading"

  return(
    <React.Fragment>
      <Grid>
        <Grid.Column floated="left" width={6}>
          <Header as='h1'>
            {title} <span> </span>
            <Transition visible={loading} animation="scale" duration={2000}>
              <Loader active={loading} size="tiny" inline as="span" className="imp-inline" />
            </Transition>
            <PaginationStats offset={offset} limit={limit} count={count} currentCount={currentCount}/>
          </Header>
        </Grid.Column>
        <Grid.Column floated="right" width={10}>
          {totalPages > 1 ? <Pagination page={page} totalPages={totalPages} handlePageChange={handlePageChange}/> : null}
          {totalPages > 1 || count > 15 ? <PerPage current={limit} handlePerPageChange={handlePerPageChange}/> : null}
        </Grid.Column>
      </Grid>
      <Table
        columns={columns}
        direction={direction}
        column={column}
        data={data}
        changeSort={changeSort}
        handleFilterChange={handleFilterChange}
        filter={filter}
        filterOptions={filterOptions}
      />
    </React.Fragment>
  )
}
