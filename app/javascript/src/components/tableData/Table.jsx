import React from "react"
import { Table as TableUI } from "semantic-ui-react"
import { TableHeader } from "./TableHeader"
import { TableBody } from "./TableBody"

export const Table = ({
  columns,
  column,
  direction,
  data,
  changeSort,
  handleFilterChange,
  filterOptions,
  filter
}) => (
  <TableUI compact sortable celled>
    <TableHeader
      columns={columns}
      column={column}
      direction={direction}
      filter={filter}
      filterOptions={filterOptions}
      changeSort={changeSort}
      handleFilterChange={handleFilterChange}
    />
    <TableBody
      data={data}
      columns={columns}
    />
  </TableUI>
)
