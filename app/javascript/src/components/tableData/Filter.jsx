import React from "react"
import { Dropdown, Input } from "semantic-ui-react"

export const Filter = ({
  name,
  title,
  type,
  value,
  options,
  onChange,
}) => {
  const preparedOptions = _.orderBy(options.map(value => ({key: value, text:value, value: value})), 'text')

  switch (type) {
    case 'action':
      return null
    case 'dropdown':
      return <Dropdown
        clearable
        onChange={(e, {value}) => { onChange(name, value, true) }}
        placeholder={`${title}...`}
        fluid
        value={value}
        search
        selection
        options={preparedOptions}
      />
    case 'eq':
      return <Input fluid value={value} placeholder={`${title}...`} onChange={({ target: { value } }) => onChange(name, value, true)}/>
    default:
      return <Input fluid value={value} placeholder={`${title}...`} onChange={({ target: { value } }) => onChange(name, value)}/>
  }
}
