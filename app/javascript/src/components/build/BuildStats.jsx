import { Table } from "semantic-ui-react"
import { useSelector } from "react-redux"
import { selectors } from "../../features/build"
import { sec2time } from "../../lib/time"

export const BuildStats = () => {
  const charge_time = sec2time(useSelector(selectors.selectChargeTime))
  const trilium_mining_power = useSelector(selectors.selectTriliumMiningPowerPercentage)
  const nft_luck = useSelector(selectors.selectNftPercentage)

  return(
    <Table compact celled columns={3}>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>Charge Time</Table.HeaderCell>
          <Table.HeaderCell>TLM</Table.HeaderCell>
          <Table.HeaderCell>NFT Luck</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>{charge_time}</Table.Cell>
          <Table.Cell>{trilium_mining_power}</Table.Cell>
          <Table.Cell>{nft_luck}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  )
}
