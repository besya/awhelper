import { Dropdown, Grid } from "semantic-ui-react"
import React from "react"

export const ToolSelector = ({
  options,
  name,
  placeholder,
  value,
  onChange,
}) => (
  <Dropdown
    clearable
    onChange={(e, {value}) => { onChange(name, value) }}
    placeholder={placeholder}
    fluid
    value={value}
    search
    selection
    options={options}
  />
)
