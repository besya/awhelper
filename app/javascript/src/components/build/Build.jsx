import { Grid, Header } from "semantic-ui-react"
import React from "react"
import { selectors as toolsSelectors } from "../../features/tools"
import { useDispatch, useSelector } from "react-redux"
import { slice, selectors } from "../../features/build"
import { ToolSelector } from "./ToolSelector"
import { ToolStats } from "./ToolStats"
import { BuildStats } from "./BuildStats"
import { getBuildParams, setBuildParams } from "../../lib/params"


const getStorageTool = (id) => localStorage.getItem(`build_tool_${id}`)

const getStorageBuild = () => {
  const tools = {}
  const tool_1 = getStorageTool(1)
  const tool_2 = getStorageTool(2)
  const tool_3 = getStorageTool(3)
  if(tool_1) tools.tool_1 = parseInt(tool_1)
  if(tool_2) tools.tool_2 = parseInt(tool_2)
  if(tool_3) tools.tool_3 = parseInt(tool_3)
  return tools
}

const getQueryBuild = () => {
  return getBuildParams()
}

export const Build = ({

}) => {
  const dispatch = useDispatch()
  const storageBuild = getStorageBuild()
  const build = useSelector(selectors.selectBuild)

  const merged = _.merge({}, build, storageBuild, getQueryBuild())

  if(!_.isEqual(build, merged)) {
    dispatch(slice.actions.set(merged))
  }

  if (_.isEmpty(getQueryBuild())) {
    setBuildParams(merged)
  }

  const exists = useSelector(selectors.selectExists)
  const allTools = useSelector(toolsSelectors.selectAll)
  const options = _.map(allTools, ({id, name, shine, rarity}) => ({ key: id, text: `${name} (${shine} / ${rarity})`, value: id }))

  const onChange = (name, id) => {
    dispatch(slice.actions.change({name, id}))
  }

  const tools = useSelector(selectors.selectTools)

  return(
    <div>
      <Header as='h1'>Your Build</Header>
      <Grid columns='equal'>
        <Grid.Row>
          <Grid.Column>
            <ToolSelector name="tool_1" value={build.tool_1} placeholder="Select Tool 1..." onChange={onChange} options={options} />
          </Grid.Column>
          <Grid.Column>
            <ToolSelector name="tool_2" value={build.tool_2} placeholder="Select Tool 2..." onChange={onChange} options={options} />
          </Grid.Column>
          <Grid.Column>
            <ToolSelector name="tool_3" value={build.tool_3} placeholder="Select Tool 3..." onChange={onChange} options={options} />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <ToolStats tool={tools.tool_1}/>
          </Grid.Column>
          <Grid.Column>
            <ToolStats tool={tools.tool_2}/>
          </Grid.Column>
          <Grid.Column>
            <ToolStats tool={tools.tool_3}/>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            {exists &&
            <BuildStats />}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  )
}
