import { Segment, Table } from "semantic-ui-react"
import React from "react"

// ["red","orange","yellow","olive","green","teal","blue","violet","purple","pink","brown","grey","black"]
const colors = {
  abundant: 'grey',
  common: 'black',
  rare: 'teal',
  epic: 'violet',
  legendary: 'yellow',
  mythical: 'red',
  null: ''
}

export const ToolStats = ({
  tool
}) => {
  if (!tool) { return null }
  return(
    <Segment>
      <Table color={colors[`${tool.rarity}`.toLowerCase()]} striped compact>
        <Table.Body>
          <Table.Row>
            <Table.Cell>Charge Time</Table.Cell>
            <Table.Cell>{tool.charge_time}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>POW</Table.Cell>
            <Table.Cell>{tool.proof_of_work_reduction}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>TLM</Table.Cell>
            <Table.Cell>{tool.trilium_mining_power}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>NFT Luck</Table.Cell>
            <Table.Cell>{tool.nft_luck}</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
    </Segment>
  )
}
