import { Grid, GridColumn } from "semantic-ui-react"
import { useSelector } from "react-redux"
import { selectors } from "../../features/online"
import React from "react"

export const Footer = () => {
  const email = "awhelpers@gmail.com"
  const year = new Date().getFullYear()
  const count = useSelector(selectors.selectCount)

  return(
    <Grid columns='2'>
      <Grid.Row>
        <GridColumn>
          Online: {count}
        </GridColumn>
        <GridColumn>
          <div style={{textAlign: 'right'}}>
            {year} &copy; Developed by <a href={`mailto: ${email}`}>Besya</a>
          </div>
        </GridColumn>
      </Grid.Row>
    </Grid>
  )
}
