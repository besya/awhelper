import React from "react"
import { slice, selectors } from "../../features/hubsTable"
import { TablePage } from "../tablePage/TablePage"

export const Hubs = ({visible}) => {
  if(!visible) { return null }

  return (
    <TablePage
      tableName="hubsTable"
      title="Mining Hubs for Your Build"
      selectors={selectors}
      actions={slice.actions}
    />
  )
}
