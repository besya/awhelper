import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { TableData } from "../tableData/TableData"
import { setTableParams } from "../../lib/params"

const calculateTotalPages = (count, limit) => Math.ceil(count/limit)

export const TablePage = ({
  tableName,
  title,
  selectors,
  actions,
}) => {
  const dispatch = useDispatch()

  const { column, direction, page, limit, columns, filter } = useSelector(selectors.selectAll)

  setTableParams(tableName, { column, direction, page, limit, filter })

  const { items, count } = useSelector(selectors.selectData)
  const { sortChanged, pageChanged, filterChanged, limitChanged } = actions
  const filterOptions = useSelector(selectors.selectFilters)
  // const status = useSelector(selectors.selectStatus)
  const status = "succeeded"

  const changeSort = c => {
    dispatch(sortChanged({ column: c }))
  }

  const changePage = currentPage => {
    dispatch(pageChanged({ page: currentPage }))
  }

  const changeFilter = (field, value, strict = false) => {
    dispatch(filterChanged({ field, value, strict }))
  }

  const changeLimit = limit => {
    dispatch(limitChanged(limit))
  }

  const handlePageChange = (p) => changePage(p)
  const handlePerPageChange = (l) => changeLimit(l)
  const handleFilterChange = (field, value, strict = false) => changeFilter(field, value, strict)

  const totalPages = calculateTotalPages(count, limit)
  const offset = useSelector(selectors.selectOffset)

  return(
    <TableData
      title={title}
      page={page}
      offset={offset}
      limit={limit}
      count={count}
      totalPages={totalPages}
      columns={columns}
      changeSort={changeSort}
      column={column}
      direction={direction}
      data={items}
      status={status}
      filterOptions={filterOptions}
      filter={filter}
      handleFilterChange={handleFilterChange}
      handlePageChange={handlePageChange}
      handlePerPageChange={handlePerPageChange}
    />
  )
}
