import React from "react"
import {useSelector} from "react-redux"
import {Header} from "./components/header/Header"
import { BrowserRouter } from "react-router-dom";
import { Router } from "./router/Router"
import { Container, Dimmer, Loader, Segment, Transition } from "semantic-ui-react"
import { selectors } from "./features/global"
import { Footer } from "./components/footer/Footer"

export const App = () => {
  const status = useSelector(selectors.selectStatus)
  const loaded = status === "succeeded"
  // if (status != 'succeeded') {
  //   return(
  //     <Segment style={{ height: '100%' }}>*/}
  //       <Dimmer active inverted>
  //         <Loader size='massive'>Loading...</Loader>
  //       </Dimmer>
  //     </Segment>
  //   )
  // }

  return(
    <React.Fragment>
      <Segment style={{ minHeight: '100%' }}>
        <Transition visible={!loaded} animation='scale' duration={500}>
            <Dimmer active={!loaded} inverted>
              <Loader size='massive'>Loading...</Loader>
            </Dimmer>
        </Transition>

          {loaded && (
            <Transition visible={loaded} animation='scale' duration={500}>
              <BrowserRouter>
                <Container fluid style={{padding: '0px'}}>
                  <Header />
                  <Router />
                  <Footer />
                </Container>
              </BrowserRouter>
            </Transition>)}

      </Segment>

        {/*)}*/}
    </React.Fragment>
  )
}
