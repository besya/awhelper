import React from "react"
import { Switch, Route, useLocation } from "react-router-dom"
import { routes } from "./routes"
// import { Transition } from "semantic-ui-react"
import { TransitionGroup, CSSTransition } from "react-transition-group"
import "../../stylesheets/animations.css"
import { NotFound } from "../pages/NotFound"

// A special wrapper for <Route> that knows how to
// handle "sub"-routes by passing them in a `routes`
// prop to the component it renders.
const RouteWithSubRoutes = (route) => {
  return (
    <Route
      path={route.path}
      render={props => {
        document.title = `AWH - ${route.title}`
        // pass the sub-routes down to keep nesting
        // <route.component {...props} routes={route.routes} />
        return(<route.component {...props} />)
      }}
    />
  )
}

export const Router = () => {
  return(
    // <TransitionGroup>
    //   <CSSTransition
    //     key={location.key}
    //     classNames="fade"
    //     timeout={1500}
    //   >
        <Switch >
          {routes.map((route, i) => (
            <RouteWithSubRoutes key={i} {...route}/>
          ))}
          <Route path="*">
            <NotFound />
          </Route>
        </Switch>
    //   </CSSTransition>
    // </TransitionGroup>
  )
}
