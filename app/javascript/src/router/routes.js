import { Dashboard } from "../pages/Dashboard"
import { Planets } from "../pages/Planets"
import { Lands } from "../pages/Lands"
import { Plots } from "../pages/Plots"
import { Tools } from "../pages/Tools"

export const routes = [
  {
    title: "Dashboard",
    path: "/",
    exact: true,
    component: Dashboard
  },
  {
    title: "Planets",
    path: "/planets",
    component: Planets,
  },
  {
    title: "Lands",
    path: "/lands",
    component: Lands,
  },
  {
    title: "Plots",
    path: "/plots",
    component: Plots,
  },
  {
    title: "Tools",
    path: "/tools",
    component: Tools,
  },
]
