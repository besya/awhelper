import React from "react"
import { slice, selectors } from "../features/landsTable"
import { TablePage } from "../components/tablePage/TablePage"

export const Lands = () => (
  <TablePage
    tableName="landsTable"
    title="Lands"
    selectors={selectors}
    actions={slice.actions}
  />
)
