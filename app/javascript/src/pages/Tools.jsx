import React from "react"
import { slice, selectors } from "../features/toolsTable"
import { TablePage } from "../components/tablePage/TablePage"

export const Tools = () => (
  <TablePage
    tableName="toolsTable"
    title="Tools"
    selectors={selectors}
    actions={slice.actions}
  />
)
