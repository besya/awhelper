import React from "react"
import { slice, selectors } from "../features/plotsTable"
import { TablePage } from "../components/tablePage/TablePage"

export const Plots = () => (
  <TablePage
    tableName="plotsTable"
    title="Plots"
    selectors={selectors}
    actions={slice.actions}
  />
)
