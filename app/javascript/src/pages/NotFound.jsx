import { Header } from "semantic-ui-react"

export const NotFound = () => <Header>Page Not Found</Header>
