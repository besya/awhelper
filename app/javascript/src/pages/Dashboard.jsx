import React from "react"
import { Build } from "../components/build/Build"
import { Hubs } from "../components/hubs/Hubs"
import { Tracks } from "../components/tracks/Tracks"
import { Divider } from "semantic-ui-react"
import { useSelector } from "react-redux"
import { selectors as tracksSelectors } from "../features/tracks"
import { selectors as buildSelectors } from "../features/build"

export const Dashboard = () => {
  const buildExists = useSelector(buildSelectors.selectExists)
  const tracksExist = !_.isEmpty(useSelector(tracksSelectors.selectIds))

  return(
    <React.Fragment>
      <Build/>
      <Divider hidden={!buildExists || !tracksExist}/>
      <Tracks visible={buildExists && tracksExist}/>
      <Divider hidden={!buildExists}/>
      <Hubs visible={buildExists}/>
    </React.Fragment>
  )
}
