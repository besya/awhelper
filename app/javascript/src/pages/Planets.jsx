import React from "react"
import { slice, selectors } from "../features/planetsTable"
import { TablePage } from "../components/tablePage/TablePage"

export const Planets = () => (
  <TablePage
    tableName="planetsTable"
    title="Planets"
    selectors={selectors}
    actions={slice.actions}
  />
)
