import createTable from "../../lib/createTable"

const name = "planetsTable"
const columns = [
  {name: 'id', title: 'ID'},
  {name: 'name', title: 'Name'},
  {name: 'current_mining_pot', title: 'Current Mining Pot'},
  {name: 'total_pool', title: 'Total Pool'},
  {name: 'max_mineable', title: 'Max Mineable'},
  {name: 'staked', title: 'Staked'},
  {name: 'percent', title: 'Percent %'},
  {name: 'fillrate', title: 'Fillrate TLM/s'},
]

export const { slice, reducer, selectors } = createTable(name, {columns})


