import { createAsyncThunk } from "@reduxjs/toolkit"
import { client } from "../../api/client"

export default createAsyncThunk(`online/fetch`, async () => {
  return await client.get(`/api/online`)
})
