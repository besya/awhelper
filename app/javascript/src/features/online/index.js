import fetch from "./fetch"
import slice from "./slice"
import selectors from "./selectors"

const reducer = slice.reducer

export {
  fetch,
  slice,
  selectors,
  reducer
}
