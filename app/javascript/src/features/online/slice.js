import { createSlice } from "@reduxjs/toolkit"
import fetch from "./fetch"

const initialState = {
  status: 'idle',
  count: 0,
  error: null
}

export default createSlice({
  name: 'online',
  initialState,
  extraReducers: {
    [fetch.pending]: (state) => {
      state.status = 'loading'
    },
    [fetch.fulfilled]: (state, action) => {
      state.count = action.payload.count
      state.status = 'succeeded'
    },
    [fetch.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    }
  }
})
