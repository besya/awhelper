import { createSelector } from "@reduxjs/toolkit"

const selectCount = createSelector(
  state => state.online,
  online => online.count
)

const selectStatus = state => state.online.status

export default {
  selectCount,
  selectStatus
}
