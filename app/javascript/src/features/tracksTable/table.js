import createTable from "../../lib/createTable"
import { slice as tracksSlice } from "../tracks"

const name = "tracksTable"
const column = "tlm_per_mine"
const direction = "ascending"
const limit = 5
const columns = [
  {name: 'planet', title: 'Planet', type: 'dropdown', width: 1},
  {name: 'land', title: 'Land', type: 'dropdown', width: 2},
  {name: 'x', title: 'X', type: 'eq', width: 1},
  {name: 'y', title: 'Y', type: 'eq', width: 1},
  {name: 'owner', title: 'Owner', width: 1},
  {name: 'commission', title: 'Commission %', width: 1},
  {name: 'current_pot', title: 'POT', width: 1},
  {name: 'tlm_per_mine', title: 'TLM per mine', width: 1},
  {name: 'tlm_per_minute', title: 'TLM per minute', width: 1},
  {name: 'nft', title: 'NFT Luck %', width: 1},
  {name: 'charge_time', title: 'Charge Time', width: 1},
  {name: 'actions', title: 'Actions', type: 'action', action: tracksSlice.actions.remove.type, actionTitle: 'Remove', width: 2 }
]

export const { slice, reducer, selectors } = createTable(name, {column, direction, columns, limit})


