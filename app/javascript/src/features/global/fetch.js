import { createAsyncThunk } from "@reduxjs/toolkit"
import { client } from "../../api/client"

export default createAsyncThunk('global/fetch', async (_, { dispatch }) => {
  const data = await client.get('/api/all')
  dispatch({ type: 'planets/fetch/fulfilled', payload: data.planets })
  dispatch({ type: 'lands/fetch/fulfilled', payload: data.lands })
  dispatch({ type: 'plots/fetch/fulfilled', payload: data.plots })
  dispatch({ type: 'tools/fetch/fulfilled', payload: data.tools })
  dispatch({ type: 'online/fetch/fulfilled', payload: data.online })
})
