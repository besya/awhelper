import { createSlice } from "@reduxjs/toolkit"
import fetch from "./fetch"

const initialState = {
  status: 'idle',
  error: null
}

export default createSlice({
  name: 'global',
  initialState,
  extraReducers: {
    [fetch.pending]: (state) => {
      state.status = 'loading'
    },
    [fetch.fulfilled]: (state) => {
      state.status = 'succeeded'
    },
    [fetch.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    }
  }
})
