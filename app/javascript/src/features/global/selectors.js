import { createSelector } from "@reduxjs/toolkit"
import { selectors as landsSelectors } from '../lands'
import { selectors as planetsSelectors } from '../planets'
import { selectors as plotsSelectors } from '../plots'
import { selectors as toolsSelectors } from '../tools'
import { selectors as onlineSelectors } from '../online'

const selectStatuses = createSelector(
  landsSelectors.selectStatus,
  planetsSelectors.selectStatus,
  plotsSelectors.selectStatus,
  toolsSelectors.selectStatus,
  onlineSelectors.selectStatus,
  (...statuses) => statuses
)

const selectLoading = createSelector(
  selectStatuses,
  (statuses) => !_.every(statuses, status => status != "loading")
)

const selectStatus = state => state.global.status

export default { selectLoading, selectStatus }
