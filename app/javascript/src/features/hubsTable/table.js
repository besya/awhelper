import createTable from "../../lib/createTable"
import { slice as tracksSlice } from "../tracks"
// import { useDispatch } from "react-redux"
// import store from "../../store"

// const track = (item) => store.dispatch(tracksSlice.actions.add(item))
// const Track = ({item}) => <a onClick={() => track(item)}>Track</a>

const name = "hubsTable"
const column = "tlm_per_mine"
const direction = "ascending"
const limit = 5
const columns = [
  // {name: 'planet', title: 'Planet', type: 'dropdown'},
  // {name: 'land', title: 'Land', type: 'dropdown', width: 2},
  // {name: 'x', title: 'X', type: 'eq', width: 1},
  // {name: 'y', title: 'Y', type: 'eq', width: 1},
  // {name: 'owner', title: 'Owner'},
  // {name: 'commission', title: 'Commission %'},
  // {name: 'land_tlm', title: 'Land TLM'},
  // {name: 'tools_tlm', title: 'Tools TLM'},
  // {name: 'hub_tlm', title: 'TLM %'},
  // {name: 'tlm', title: 'TLM %'},
  // {name: 'tlm_per_mine', title: 'TLM per mine'},
  // {name: 'tlm_per_minute', title: 'TLM per minute'},
  // // {name: 'land_charge_time', title: 'Land Charge Time'},
  // // {name: 'tools_charge_time', title: 'Tools Charge Time'},
  // {name: 'nft', title: 'NFT Luck %'},
  // {name: 'charge_time', title: 'Charge Time'},
  {name: 'planet', title: 'Planet', type: 'dropdown', width: 1},
  {name: 'land', title: 'Land', type: 'dropdown', width: 2},
  {name: 'x', title: 'X', type: 'eq', width: 1},
  {name: 'y', title: 'Y', type: 'eq', width: 1},
  {name: 'owner', title: 'Owner', width: 1},
  {name: 'commission', title: 'Commission %', width: 1},
  {name: 'current_pot', title: 'POT', width: 1},
  {name: 'tlm_per_mine', title: 'TLM per mine', width: 1},
  {name: 'tlm_per_minute', title: 'TLM per minute', width: 1},
  {name: 'nft', title: 'NFT Luck %', width: 1},
  {name: 'charge_time', title: 'Charge Time', width: 1},

  {name: 'actions', title: 'Actions', type: 'action', action: tracksSlice.actions.add.type, actionTitle: 'Track', width: 2 }
]

export const { slice, reducer, selectors } = createTable(name, {column, direction, columns, limit})


