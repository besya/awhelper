import createTable from "../../lib/createTable"

const name = "plotsTable"
const columns = [
  {name: 'id', title: 'ID', type: 'eq'},
  {name: 'planet_name', title: 'Planet', type: 'dropdown'},
  {name: 'land_name', title: 'Land', type: 'dropdown' },
  {name: 'rarity', title: 'Rarity', type: 'dropdown' },
  {name: 'mint', title: 'Mint' },
  {name: 'owner_profit_share_from_mining', title: 'Commission %' },
  {name: 'coordinate_x', title: 'X' },
  {name: 'coordinate_y', title: 'Y' },
  {name: 'land_owned_by', title: 'Owner' },
]

export const { slice, reducer, selectors } = createTable(name, {columns})


