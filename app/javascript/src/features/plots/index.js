import createEntity from "../../lib/createEntity"

export const {
  fetch,
  adapter,
  slice,
  selectors,
  reducer
} = createEntity('plots')
