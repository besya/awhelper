import createTable from "../../lib/createTable"

const name = "landsTable"
const columns = [
  {name: 'id', title: 'ID', width: 1, type: 'eq'},
  {name: 'name', title: 'Name'},
  {name: 'charge_time', title: 'Charge time'},
  {name: 'proof_of_work_reduction', title: 'Proof of work reduction'},
  {name: 'trilium_mining_power', title: 'Trilium mining power'},
  {name: 'nft_luck', title: 'NFT luck'},
]

export const { slice, reducer, selectors } = createTable(name, {columns})


