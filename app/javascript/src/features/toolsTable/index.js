import { slice, reducer, selectors as tableSelectors } from "./table"
import { selectors as dataSelectors } from "../tools"
import { selectFilterOptions, selectTableData } from "../../lib/tableDataSelectors"

export {
  slice,
  reducer
}

export const selectors = {
  ...tableSelectors,
  selectData: selectTableData(dataSelectors.selectAll, tableSelectors.selectAll),
  selectFilters: selectFilterOptions(dataSelectors.selectAll, tableSelectors.selectAll)
}
