import createTable from "../../lib/createTable"

const name = "toolsTable"
const column = "original_id"
const columns = [
  {name: 'original_id', title: 'ID', width: 1, type: 'eq'},
  {name: 'name', title: 'Name', width: 2},
  {name: 'rarity', title: 'Rarity', type: 'dropdown'},
  {name: 'max_issuance', title: 'Max issuance'},
  {name: 'shine', title: 'Shininess', type: 'dropdown'},
  {name: 'type', title: 'Type', type: 'dropdown'},
  {name: 'charge_time', title: 'Charge time', type: 'eq'},
  {name: 'proof_of_work_reduction', title: 'POW', type: 'eq'},
  {name: 'trilium_mining_power', title: 'TLM', type: 'eq'},
  {name: 'nft_luck', title: 'NFT luck', type: 'eq'},
  {name: 'tlm_efficiency', title: 'TLM efficiency %'},
  {name: 'nft_efficiency', title: 'NFT efficiency %'},
  {name: 'total_efficiency', title: 'Total efficiency %'},
]

export const { slice, reducer, selectors } = createTable(name, {column, columns})


