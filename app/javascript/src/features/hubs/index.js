import { createSelector } from "@reduxjs/toolkit"
import { selectors as buildSelectors } from "../build"
import { selectors as plotsSelectors } from "../plots"
import { selectors as landsSelectors } from "../lands"
import { selectors as planetsSelectors } from "../planets"
import {
  mineChargeTime, mineNftChancePercentage,
  mineTriliumAmount,
  mineTriliumAmountWithCommission,
  mineTriliumPercentage,
  round
} from "../../lib/math"
import { sec2time } from "../../lib/time"

const selectAll = createSelector(
  planetsSelectors.selectEntities,
  landsSelectors.selectEntities,
  plotsSelectors.selectAll,
  buildSelectors.selectChargeTime,
  buildSelectors.selectTriliumMiningPowerPercentage,
  buildSelectors.selectNftPercentage,
  (planets, lands, plots, toolsChargeTime, toolsTlm, toolsNft) => (
    plots.map(plot => {
      const planet = planets[plot.planet_id]
      const land = lands[plot.land_id]
      const landTlm = land.trilium_mining_power
      const landChargeTime = land.charge_time
      const currentMiningPot = planet.current_mining_pot
      const commission = plot.owner_profit_share_from_mining
      const mineTlmPercentage = mineTriliumPercentage(toolsTlm, landTlm)
      const mineTlmAmount = mineTriliumAmount(mineTlmPercentage, currentMiningPot)
      const mineTlmAmountWithCommission = mineTriliumAmountWithCommission(mineTlmAmount, commission)
      const chargeTime = mineChargeTime(toolsChargeTime, landChargeTime)
      const mineNftChance = mineNftChancePercentage(toolsNft, land.nft_luck)
      const mineTlmPerMinute = mineTlmAmountWithCommission / chargeTime * 60

      return {
        id: plot.id,
        rarity: plot.rarity,
        planet: planet.name,
        land: land.name,
        x: plot.coordinate_x,
        y: plot.coordinate_y,
        owner: plot.land_owned_by,
        commission: commission,
        nft: mineNftChance,
        current_pot: currentMiningPot,
        land_tlm: landTlm,
        tools_tlm: toolsTlm,
        hub_tlm: mineTlmPercentage,
        tlm: mineTlmAmount,
        tlm_per_mine: mineTlmAmountWithCommission,
        tlm_per_minute: round(mineTlmPerMinute, 8),
        land_charge_time: landChargeTime,
        tools_charge_time: toolsChargeTime,
        charge_time: sec2time(chargeTime),
      }
    })
  )
)

const selectors = {
  selectAll
}

export {
  selectors
}
