import { createSlice } from "@reduxjs/toolkit"
import _ from "lodash"
import { getBuildParams, setBuildParams } from "../../lib/params"

const getItem = (name) => parseInt(localStorage.getItem(`build_${name}`))

const saveTool = (id, value) => {
  if(value) {
    localStorage.setItem(`build_tool_${id}`, value)
  } else {
    localStorage.removeItem(`build_tool_${id}`)
  }
}
const getTool = name => getItem(name) || null
const getStorageTool = (id) => localStorage.getItem(`build_tool_${id}`)

const saveBuild = (build) => {
  saveTool(1, build.tool_1)
  saveTool(2, build.tool_2)
  saveTool(3, build.tool_3)
}

const getStorageBuild = () => {
  const tools = {}
  const tool_1 = getStorageTool(1)
  const tool_2 = getStorageTool(2)
  const tool_3 = getStorageTool(3)
  if(tool_1) tools.tool_1 = parseInt(tool_1)
  if(tool_2) tools.tool_2 = parseInt(tool_2)
  if(tool_3) tools.tool_3 = parseInt(tool_3)
  return tools
}

const initialState = {
  tool_1: null,
  tool_2: null,
  tool_3: null
}

export default createSlice({
  name: 'build',
  initialState: initialState,
  reducers: {
    change: (state, action) => {
      const { name, id } = action.payload
      state[name] = id
      saveBuild(state)
      setBuildParams(state)
    },
    set: (state, action) => {
      state.tool_1 = action.payload.tool_1
      state.tool_2 = action.payload.tool_2
      state.tool_3 = action.payload.tool_3
      saveBuild(state)
      setBuildParams(state)
    }
  }
})
