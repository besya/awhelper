import slice from "./slice"
import reducer from "./reducer"
import selectors from "./selectors"

export {
  slice,
  reducer,
  selectors
}
