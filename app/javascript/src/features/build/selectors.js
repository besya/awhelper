import { createSelector } from "@reduxjs/toolkit"
import { selectors } from "../tools"
import { toolsChargeTime, toolsNftPercentage, toolsTriliumMiningPowerPercentage } from "../../lib/math"

const selectBuild = state => state.build

const selectTool = (id) => createSelector(
  state => state,
  state => selectors.selectById(state, id)
)

const selectTools = createSelector(
  state => state,
  selectBuild,
  (state, build) => ({
    tool_1: selectors.selectById(state, build.tool_1),
    tool_2: selectors.selectById(state, build.tool_2),
    tool_3: selectors.selectById(state, build.tool_3),
  })
)

const selectCompactTools = createSelector(
  selectTools,
  tools => _.chain(tools).values().compact().value()
)

const selectExists = createSelector(
  selectCompactTools,
  tools => tools.length > 0
)

const selectChargeTimes = createSelector(
  selectCompactTools,
  tools => _.chain(tools).map('charge_time').value()
)

const selectChargeTime = createSelector(
  selectChargeTimes,
  values => toolsChargeTime(values)
)

const selectTriliumMiningPowerPercentages = createSelector(
  selectCompactTools,
  tools => _.chain(tools).map('trilium_mining_power').value()
)

const selectTriliumMiningPowerPercentage = createSelector(
  selectTriliumMiningPowerPercentages,
  values => toolsTriliumMiningPowerPercentage(values)
)

const selectNftPercentages = createSelector(
  selectCompactTools,
  tools => _.chain(tools).map('nft_luck').value()
)

const selectNftPercentage = createSelector(
  selectNftPercentages,
  values => toolsNftPercentage(values)
)


export default {
  selectBuild,
  selectTool,
  selectTools,
  selectCompactTools,
  selectExists,
  selectChargeTimes,
  selectChargeTime,
  selectTriliumMiningPowerPercentages,
  selectTriliumMiningPowerPercentage,
  selectNftPercentages,
  selectNftPercentage,
}

