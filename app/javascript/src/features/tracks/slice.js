import { createSlice } from "@reduxjs/toolkit"

const getIds = () => {
  const data = localStorage.getItem(`tracks`)

  if(data) {
    return data.split(',').map(i => parseInt(i))
  } else {
    return []
  }
}
const getItems = () => getIds().map(id => ({id})) || []
const saveItems = (items) => localStorage.setItem(`tracks`, items)


const initialState = {
  items: getItems()
}

const ids = items => items.map(item => item.id)
const exist = (items, id) => ids(items).includes(id)

export default createSlice({
  name: 'tracks',
  initialState,
  reducers: {
    add: (state, action) => {
      if (!exist(state.items, action.payload.id)){
        state.items.push({id: action.payload.id})
        saveItems(ids(state.items))
      }
    },
    remove: (state, action) => {
      _.remove(state.items, item => item.id == action.payload.id)
      saveItems(ids(state.items))
    }
  }
})
