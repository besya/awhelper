import { createSelector } from "@reduxjs/toolkit"
import { selectors } from "../hubs"

const selectTracks = state => state.tracks

const selectIds = createSelector(
  selectTracks,
  tracks => tracks.items.map(item => item.id)
)

const selectAll = createSelector(
  selectIds,
  selectors.selectAll,
  (tracks, hubs) => _.filter(hubs, hub => tracks.includes(hub.id))
)

export default {
  selectIds,
  selectAll
}
