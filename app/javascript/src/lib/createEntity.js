import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit"
import { client } from "../api/client"
import { isArrayEqual } from "./array"
import { original } from "immer"

export default (name) => {
  const adapter = createEntityAdapter()
  const initialState = adapter.getInitialState({status: 'idle', error: null})
  const selectors = {
    ...adapter.getSelectors(state => state[name]),
    selectStatus: state => state[name].status
  }

  const fetch = createAsyncThunk(`${name}/fetch`, async () => {
    return await client.get(`/api/${name}`)
  })

  const fetchReducers = {
    [fetch.pending]: (state) => {
      state.status = 'loading'
    },
    [fetch.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      // if (isArrayEqual(action.payload, _.values(original(state).entities))) {
      //   return
      // }
      adapter.upsertMany(state, action)
    },
    [fetch.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.error.message
    }
  }

  const slice = createSlice({ name, initialState, extraReducers: fetchReducers})
  const reducer = slice.reducer

  return {
    fetch,
    adapter,
    slice,
    reducer,
    selectors: {
      ...selectors,

    },
  }
}
