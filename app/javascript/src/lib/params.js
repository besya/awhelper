import queryString from 'query-string'

const getUrlString = () => window.location.href
const getUrl = () => new URL(getUrlString())
const getParam = (url, name) => url.searchParams.get(name)
const setParam = (url, name, value) => {
  url.searchParams.set(name, value)
  return url
}

const getTableParams = (table) => {
  const parsed = queryString.parse(location.search, {parseBooleans: true, parseNumbers: true})

  const limit = parsed[`${table}_limit`]
  const page = parsed[`${table}_page`]
  const column = parsed[`${table}_column`]
  const direction = parsed[`${table}_direction`]
  let filter = parsed[`${table}_filter`]

  if (filter) {
    filter = Array.isArray(filter) ? filter : [filter]
    filter = _.merge(..._.map(filter, (string) => { const a = string.split(','); return { [a[0]]: {value: a[1], strict: a[2] == 'true' } } }))
  }
  return { limit, page, column, direction, filter }
}

const setTableParams = (table, state) => {
  const params = location.search
  let parsed = queryString.parse(params, {parseBooleans: true, parseNumbers: true})
  const { limit, page, column, direction, filter } = state

  const query = { limit, page, column, direction, filter: _.map(filter, ({value, strict}, key) => `${key},${value},${strict}`) }

  if(!_.isEmpty(query.filter)) {
    parsed[`${table}_filter`] = []
  }

  const string = queryString.stringify(_.merge({}, parsed, _.mapKeys(query, (value, key) => `${table}_${key}`)))

  const url = new URL(window.location.href)
  url.search = string
  window.history.replaceState({}, '', url.href)
}

const setBuildParams = (state) => {
  const url = getUrl()
  const { tool_1, tool_2, tool_3 } = state
  setParam(url, `tool_1`, tool_1)
  setParam(url, `tool_2`, tool_2)
  setParam(url, `tool_3`, tool_3)
  window.history.replaceState({}, '', url.href)
}

const getBuildParams = () => {
  const url = getUrl()
  const tool_1 = parseInt(getParam(url, `tool_1`)) || null
  const tool_2 = parseInt(getParam(url, `tool_2`)) || null
  const tool_3 = parseInt(getParam(url, `tool_3`)) || null
  const state = {}
  if (tool_1 || tool_2 || tool_3) {
    state.tool_1 = tool_1
    state.tool_2 = tool_2
    state.tool_3 = tool_3
  }
  return state
}

export {
  getUrlString,
  getUrl,
  getParam,
  setParam,
  getTableParams,
  setTableParams,
  getBuildParams,
  setBuildParams
}
