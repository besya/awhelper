const round = (number, count = 2) => {
  let multiplier = 1

  if (count > 1) {
    multiplier = parseInt('1'.concat('0'.repeat(count)))
  }

  return Math.round((number + Number.EPSILON) * multiplier) / multiplier
}

// Sum of Trilium Mining Power percentages of the Tools NFTs
const toolsTriliumMiningPowerPercentage = (values) => _.chain(values).sum().value()

// % of the Planet’s mining Current Mining Pot at time of mine awarded to miner =
// (Sum of Trilium Mining Power percentages of the Tools NFTs) * Trilium Mining Power Multiplier of the Land NFT
const mineTriliumPercentage = (
  toolsTriliumMiningPowerPercentage,
  landTriliumMiningPowerMultiplier
) => round(toolsTriliumMiningPowerPercentage * landTriliumMiningPowerMultiplier, 2)

const mineTriliumAmount = (
  mineTriliumPercentage,
  currentMiningPot
) => round((mineTriliumPercentage / 100) * currentMiningPot, 8)

// Commission to Landowner
// Land is owned by a landowner if it has been bought (or held by a Federation contract if
// unbought). The landowner may choose to extract a commission from miners, in which case this
// is deducted automatically. By default, the commission is set to 20% on all Land NFTs. The
// commission on Federation-owned land is held in a reserve.
const mineTriliumAmountWithCommission = (
  mineTriliumAmount,
  commission
) => round(mineTriliumAmount - mineTriliumAmount * (commission / 100), 8)

// NFTs Paid Out

// sum of Luck percentages of Tools NFTs
const toolsNftPercentage = (values) => round(_.chain(values).sum().value(), 2)

//  Stage 1
//    % chance of clearing stage 1 = (sum of Luck percentages of Tools NFTs) * Luck Multiplier of Land
const mineNftChancePercentage = (
  toolsNftPercentage,
  landLuckMultiplier
) => round(toolsNftPercentage * landLuckMultiplier, 2)


// Charge Time
// Tools charge time
//  If three tools are being used, the longest 2 charge times are added to derive the
//  charge time. If two tools are used, the longer charge time and half of the shorter
//  charge time are added together to give the total time that must have elapsed
//  since last mine
const toolsChargeTime = (values) => {
  const times = _.chain(values).sort().reverse().value()
  let result = 0

  if (values.length == 3) {
    result = _.chain(times).take(2).sum().value()
  } else if (values.length == 2) {
    result = times[0] + (times[1] / 2)
  } else {
    result = times[0]
  }

  return parseInt(result)
}

// The charge time in seconds given by the Tools is multiplied by the Land’s charge time multiplier
const mineChargeTime = (
  toolsChargeTime,
  landChargeTimeMultiplier
) => parseInt(toolsChargeTime * landChargeTimeMultiplier)

export {
  round,
  toolsTriliumMiningPowerPercentage,
  toolsNftPercentage,
  toolsChargeTime,
  mineTriliumPercentage,
  mineTriliumAmount,
  mineTriliumAmountWithCommission,
  mineNftChancePercentage,
  mineChargeTime,
}
