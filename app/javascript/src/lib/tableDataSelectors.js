import { createSelector } from "@reduxjs/toolkit"
import _ from "lodash"
import createCollection from "./createCollection"

export const selectTableData = (dataSelector, tableSelector) => createSelector(
  dataSelector,
  tableSelector,
  (items, filter) => createCollection(items, filter)
)


export const selectFilterOptions = (dataSelector, tableSelector) => createSelector(
  dataSelector,
  tableSelector,
  (items, table) => (
    _.chain(table.columns)
      .filter(['type', 'dropdown'])
      .map(({name}) => ({name, options: _.uniq(_.map(items, name))}))
      .keyBy('name')
      .value()
  )
)
