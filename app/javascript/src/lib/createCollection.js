import _ from "lodash"
import calculateOffset from "./calculateOffset"

const applySort = (collection, column, direction) => {
  let ordered = _.sortBy(collection, [column])
  return direction == 'ascending' ? ordered.reverse() : ordered
}

const applyPagination = (collection, page, limit) => {
  const offset = calculateOffset(page, limit)
  return collection.slice(offset, offset + limit)
}

const filterPredicateLike = (item, field, value) => {
  return `${item[field]}`.toLowerCase().includes(value.toLowerCase())
}

const filterPredicateEq = (item, field, value) => {
  return `${item[field]}`.toLowerCase() === `${value}`.toLowerCase()
}

const applyFilter = (collection, filter) => {
  return _.filter(collection, (item) => {
    return _.every(filter, ({value, strict}, field) => {
      if(strict) {
        return filterPredicateEq(item, field, value)
      }

      return filterPredicateLike(item, field, value)
    })
  })
}

export default (collection, { column, direction, limit, page, filter }) => {
  const filtered = _.isEmpty(filter) ? collection : applyFilter(collection, filter)
  const ordered = applySort(filtered, column, direction)
  const paginated = applyPagination(ordered, page, limit)

  return {
    items: paginated,
    count: filtered.length
  }
}
