export const sec2time = (timeInSeconds) => {
  const pad = (num, size) => { return ('000' + num).slice(size * -1); }
  const time = parseFloat(timeInSeconds).toFixed(3)
  const hours = Math.floor(time / 60 / 60)
  const minutes = Math.floor(time / 60) % 60
  const seconds = Math.floor(time - minutes * 60)
  // const milliseconds = time.slice(-3)

  // return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2) + ',' + pad(milliseconds, 3);
  return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2)
}
