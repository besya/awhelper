export default (page, limit) => (page - 1) * limit
