import _ from "lodash"
import { createSelector, createSlice } from "@reduxjs/toolkit"
import calculateOffset from "./calculateOffset"
import { getTableParams, setTableParams } from "./params"

const emptyState = {
  columns: [],
  column: "id",
  direction: "descending",
  limit: 15,
  page: 1,
  filter: {}
}

const reducers = (name) => ({
  pageChanged: (state, action) => {
    state.page = action.payload.page
    setTableParams(name, state)
  },
  sortChanged: (state, action) => {
    if (state.column === action.payload.column) {
      state.direction = state.direction === 'ascending' ? 'descending' : 'ascending'
    } else {
      state.column = action.payload.column
      state.direction = 'descending'
    }
    setTableParams(name, state)
  },
  filterChanged: (state, action) => {
    const {field, value, strict} = action.payload
    value ? state.filter[field] = {value, strict} : (delete state.filter[field])
    state.page = 1
    setTableParams(name, state)
  },
  limitChanged: (state, action) => {
    state.page = 1
    state.limit = action.payload
    setTableParams(name, state)
  }
})

export default (name, defaultState) => {
  const initialState =  _.merge({}, emptyState, defaultState, getTableParams(name))
  const slice = createSlice({ name, initialState, reducers: reducers(name)})
  const reducer = slice.reducer

  const selectors = {
    selectAll: state => state[name],
    selectOffset: createSelector(state => state[name], ({page, limit}) => calculateOffset(page, limit))
  }

  return {
    slice,
    reducer,
    selectors,
  }
}
