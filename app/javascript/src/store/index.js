import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { reducer as globalReducer } from "../features/global"
import { reducer as planetsReducer } from "../features/planets"
import { reducer as landsReducer } from "../features/lands"
import { reducer as plotsReducer } from "../features/plots"
import { reducer as toolsReducer } from "../features/tools"
import { reducer as tracksReducer } from "../features/tracks"
import { reducer as landsTableReducer } from "../features/landsTable"
import { reducer as planetsTableReducer } from "../features/planetsTable"
import { reducer as plotsTableReducer } from "../features/plotsTable"
import { reducer as toolsTableReducer } from "../features/toolsTable"
import { reducer as buildTableReducer } from "../features/build"
import { reducer as hubsTableReducer } from "../features/hubsTable"
import { reducer as tracksTableReducer } from "../features/tracksTable"
import { reducer as onlineReducer } from "../features/online"

const logMiddleware = store => next => action => {
  // console.log('Dispatching', action)
  return next(action)
}

export default configureStore({
  reducer: {
    global: globalReducer,
    planets: planetsReducer,
    lands: landsReducer,
    plots: plotsReducer,
    tools: toolsReducer,
    tracks: tracksReducer,
    landsTable: landsTableReducer,
    planetsTable: planetsTableReducer,
    plotsTable: plotsTableReducer,
    toolsTable: toolsTableReducer,
    build: buildTableReducer,
    hubsTable: hubsTableReducer,
    tracksTable: tracksTableReducer,
    online: onlineReducer,
  },
  middleware: [
    logMiddleware, ...getDefaultMiddleware()
  ]
})
