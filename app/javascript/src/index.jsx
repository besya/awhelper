import ReactDOM from 'react-dom'
import { App } from "./App";

import { Provider } from "react-redux"
import store from "./store"

import { fetch } from "./features/global"
import { fetch as fetchPlots } from "./features/plots"
import { fetch as fetchPlanets } from "./features/planets"
import { fetch as fetchOnline } from "./features/online"

store.dispatch(fetch())

const updatePlanets = () => {
  const dispatch = store.dispatch
  const state = store.getState()

  if(state.planets.status === 'succeeded') {
    dispatch(fetchPlanets())
  }
}

const updatePlots = () => {
  const dispatch = store.dispatch
  const state = store.getState()

  if(state.plots.status === 'succeeded') {
    dispatch(fetchPlots())
  }
}

const updateOnline = () => {
  const dispatch = store.dispatch
  const state = store.getState()

  if(state.online.status === 'succeeded') {
    dispatch(fetchOnline())
  }
}

setInterval(updateOnline, 5 * 1000)
setInterval(updatePlanets, 5 * 1000)
setInterval(updatePlots, 5 * 60 * 1000)

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.querySelector("#root")
  )
})
