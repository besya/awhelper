class BuildPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.by_user(user).all
    end
  end

  def show?
    users_record
  end

  def edit?
    users_record
  end

  def update?
    users_record
  end

  def destroy?
    users_record
  end

  private

  def users_record
    record.user == user
  end
end
