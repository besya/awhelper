class ApiController < ApplicationController
  after_action :log_visit

  def log_visit
    Online.add request.session.id
  end
end

