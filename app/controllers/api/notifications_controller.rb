module Api
  class NotificationsController < ApiController
    Notification = Struct.new(:id, :message, :date)

    NOTIFICATIONS = [
      Notification.new(1, 'Hello', Time.now)
    ]

    def index
      render json: NOTIFICATIONS
    end
  end
end
