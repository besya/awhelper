module Api
  class PlotsController < ApiController
    def index
      render json: PlotsCache.fetch
    end
  end
end
