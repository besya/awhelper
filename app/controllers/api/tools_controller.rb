module Api
  class ToolsController < ApiController
    def index
      render json: ToolsCache.fetch
    end
  end
end
