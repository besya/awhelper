module Api
  class ToolTypesController < ApiController
    def index
      render json: ToolTypesCache.fetch
    end
  end
end
