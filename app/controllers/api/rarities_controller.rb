module Api
  class RaritiesController < ApiController
    def index
      render json: RaritiesCache.fetch
    end
  end
end
