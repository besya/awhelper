module Api
  class LandsController < ApiController
    def index
      render json: LandsCache.fetch
    end
  end
end
