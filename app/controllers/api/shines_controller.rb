module Api
  class ShinesController < ApiController
    def index
      render json: ShinesCache.fetch
    end
  end
end
