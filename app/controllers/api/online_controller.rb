module Api
  class OnlineController < ApiController
    def index
      render json: { count: Online.count }
    end
  end
end
