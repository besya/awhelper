module Api
  class AllController < ApiController
    def index
      render json: data
    end

    def data
      "{
        \"planets\": #{PlanetsCache.fetch},
        \"lands\": #{LandsCache.fetch},
        \"plots\": #{PlotsCache.fetch},
        \"tools\": #{ToolsCache.fetch},
        \"online\": {\"count\":#{Online.count}}
      }"
    end
  end
end
