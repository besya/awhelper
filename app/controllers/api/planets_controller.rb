module Api
  class PlanetsController < ApiController
    def index
      render json: PlanetsCache.fetch
    end
  end
end
