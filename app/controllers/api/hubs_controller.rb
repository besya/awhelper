module Api
  class HubsController < ::ApiController
    def index
      render json: MiningHub.by_user(current_user).all
    end
  end
end
