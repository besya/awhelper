require 'open-uri'

class AwstatsLands
  URL = "https://www.awstats.io/land/table"

  def lands
    page.css("#table tbody tr").map do |tr|
      tr.css("td").map { |td| td.inner_text }
    end
  end

  def page
    @page ||= Nokogiri::HTML.parse(URI.open(URL))
  end
end
