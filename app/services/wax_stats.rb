require 'open-uri'

# http://wax.stats.eosusa.news/d/g-n07fbGz/alienworlds-planet-info?orgId=1&refresh=60s
class WaxStats
  def initialize(step = 1.minute)
    @step = step.to_i
  end

  def get
    pots_hash = JSON.parse(URI.open(pot_url).read)
    # stack_hash = JSON.parse(URI.open(stack_url).read)
    # p pot_url
    pots = parse(pots_hash)
    # stacks = parse(stack_hash)
    # pots.map { |planet, pot| {planet: planet, pot: pot, stacked: stacks[planet]} }
    pots
  rescue
    {}
  end

  def parse(hash)
    hash["data"]["result"].map do |result|
      [result["metric"]["world"].underscore, result["values"].last.last]
    end.to_h
  end

  def stack_url
    "http://wax.stats.eosusa.news/api/datasources/proxy/1/api/v1/query_range?query=alienworlds_planet_staked%7Bworld%3D~%22Eyeke%7CKavian%7CMagor%7CNaron%7CNeri%7CVeles%22%7D&start=#{start_time}&end=#{end_time}&step=15"
  end

  def pot_url
    "http://wax.stats.eosusa.news/api/datasources/proxy/1/api/v1/query_range?query=alienworlds_planet_minemax%7Bworld%3D~%22Eyeke%7CKavian%7CMagor%7CNaron%7CNeri%7CVeles%22%7D&start=#{start_time}&end=#{end_time}&step=60"
  end

  def start_time
    (Time.now - @step).to_i
  end

  def end_time
    Time.now.to_i
  end
end
