require 'open-uri'

class AwstatsPools
  URL = "https://www.awstats.io/mining/pools"

  def mining_pools
    page.css("#mining tbody tr").map do |tr|
      data = tr.css("td").map { |td| td.inner_text }
      planet_name, total_pool, mining_pool, max_mineable, fillrate = data
      [planet_name.underscore, {
        total_pool: total_pool.delete(',').to_i,
        current_mining_pot: mining_pool.to_f,
        max_mineable: max_mineable.to_f,
        fillrate: fillrate.to_f
      }]
    end.to_h
  end

  def staking
    page.css("#staking tbody tr").map do |tr|
      data = tr.css("td").map { |td| td.inner_text }
      planet_name, staked = data
      [planet_name.underscore, {
        staked: staked.delete(',').to_i,
      }]
    end.to_h
  end

  def all
    mining_pools.deep_merge staking
  end

  def page
    @page ||= Nokogiri::HTML.parse(URI.open(URL))
  rescue
    Nokogiri::HTML::Document.new
  end
end
