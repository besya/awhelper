module Online
  SET = "online"
  WINDOW = 15.minutes

  class << self
    def add(ip)
      $redis.zadd(SET, Time.now.to_i.to_s, ip)
    end

    def count
      $redis.zcount(SET, WINDOW.ago.to_i.to_s, Time.now.to_i.to_s)
    end

    def clear
      $redis.zremrangebyscore SET, 0.to_s, WINDOW.ago.to_i.to_s
    end
  end
end
