class Recommendations
  class Pool
    # @return [Array<Mine>]
    attr_reader :items
    attr_reader :limit, :order_field, :order_direction

    def initialize(limit:, order_field:, order_direction:)
      @limit = limit
      @order_field = order_field
      @order_direction = order_direction
      @items = []
    end

    def full?
      items.size == limit
    end

    def compare(a, b)
      left = a.send(order_field.to_sym)
      right = b.send(order_field.to_sym)
      order_direction.to_sym == :desc ? right <=> left : left <=> right
    end

    def reorder!
      @items = items.sort(&method(:compare))
    end

    def touch
      items.last
    end

    # @param item [Mine]
    def push(item)
      unless full?
        items.push item
        reorder!
        return
      end

      if compare(touch, item) == 1
        items.pop
        push(item)
      end
    end
  end

  # @return [Build]
  attr_reader :build

  # @return [Pool]
  attr_reader :pool

  def initialize(build:, order_field:, order_direction:, limit: 10)
    @build = build
    @pool = Pool.new(limit: limit, order_field: order_field, order_direction: order_direction)
  end

  def fetch
    Plot.includes(:land, :planet, :rarity).find_each do |plot|
      pool.push Mine.new(build: build, plot: plot)
    end

    pool.items
  end
end
