class BestQuery
  class Pool
    # @return [Array<MiningHub>]
    attr_reader :pool
    attr_reader :limit

    def initialize(limit = 10)
      @limit = limit
      @pool = []
    end

    def full?
      pool.size == limit
    end

    def reorder!
      @pool = pool.sort_by(&:trilium_per_mine).reverse
    end

    def touch
      pool.first
    end

    def push(item)
      unless full?
        @pool.push(item)
        reorder!
        return
      end

      if touch.trilium_per_mine < item.trilium_per_mine
        @pool.pop
        push(item)
        reorder!
      end
    end
  end

  class PoolItem
    # @return [MiningHub]
    attr_reader :build

    # @return [Plot]
    attr_reader :plot

    # @return [MineCalculator]
    attr_reader :calculator

    delegate :trilium_per_mine, :charge_time, :nft_luck, to: :calculator

    def initialize(build, plot)
      @build = build
      @plot = plot
      init
    end

    def init
      @calculator = MineCalculator.new(
        land_trilium_mining_power: plot.land.trilium_mining_power,
        tools_trilium_mining_power: build.tools_trilium_mining_power,
        commission: plot.owner_profit_share_from_mining,
        planet_current_mining_pot: plot.planet.current_mining_pot,
        land_nft_luck: plot.land.nft_luck,
        tools_nft_luck: build.tools_nft_luck,
        land_charge_time: plot.land.charge_time,
        tools_charge_time: build.tools_charge_time
      )
    end
  end

  # @return [MiningHub]
  attr_reader :build

  def initialize(build)
    @build = build
    @pool = Pool.new(10)
  end

  def fetch
    Plot.includes(:land, :planet).find_each do |plot|
      @pool.push PoolItem.new(build, plot)
    end

    @pool.pool
  end
end
