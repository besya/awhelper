# README

## Development

### Prepare
`bundle install`

`rake db:create`

`rake db:migrate`

`rake db:seed` or `rake db:seed:replant`

### Run using Heroku

`heroku local`

### Run classic

`bundle exec puma -t 5:5 -p 3000 -e development`

`clockwork config/clockwork.rb`

### React hot reloading

`bin/webpack-dev-server`

### Cache useful tips

`rake cache:clear`

```
rails c
PlanetsCache.read
PlanetsCache.clear
PlanetsCache.write
```

### Fetching data

`rake fetch:planets`

`rake fetch:plots`

## Deployment

### Flow

#### CI/CD
1. Push the feature branch
2. Create a merge request of the feature branch to master
3. Merge
4. CI/CD automatically deploys master branch to `staging`

#### Release to production
1. Create tag
2. CI/CD automatically deploys this tag to `production`

## Heroku

### Logs
#### Production
`heroku logs -t`
#### Staging
`heroku logs -t -a awhelper-staging`

### Rails console

#### Production
`heroku run rails c`
#### Staging
`heroku run rails c -a awhelper-staging`

### Rake tasks

#### Production
`heroku run rake fetch:planets`
#### Staging
`heroku run rake db:migrate -a awhelper-staging`

### Heroku instance

#### Show dynos

`heroku ps`

### Manual deploy

`git push heroku master`
