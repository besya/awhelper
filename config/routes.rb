Rails.application.routes.draw do
  root 'home#index'
  # resources :builds
  # devise_for :users
  # ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
    get 'all', to: 'all#index'
    get 'online', to: 'online#index'
    # resources :hubs
    # resources :lands
    # resources :notifications
    resources :planets
    resources :plots
    # resources :rarities
    # resources :shines
    # resources :tool_types
    # resources :tools
  end

  get '*path', to: 'home#index'
end
