if ENV['RAILS_ENV'] == "production"
  $redis = Redis.new(url: ENV['REDIS_URL'], ssl_params: { verify_mode: OpenSSL::SSL::VERIFY_NONE })
else
  $redis = Redis.new
end
