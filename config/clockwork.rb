require 'clockwork'
require 'active_support/time' # Allow numeric durations (eg: 1.minutes)
require_relative './boot'
require_relative './environment'

module Clockwork
  every(Online::WINDOW, 'clean inactive online') { Online.clear }

  # every(5.seconds, 'fetch_mining_pot.graphana') do
  #   stats = WaxStats.new(5.seconds).get
  #   unless stats.empty?
  #     stats.each do |name, pot|
  #       pot = pot.to_f > 0 ? pot.to_f : 0
  #       Planet.where(name: name).update_all(current_mining_pot: pot)
  #     end
  #     PlanetsCache.write
  #   end
  # end

  every(3.seconds, 'fetch_planets.awstats') do
    stats = AwstatsPools.new.all

    unless stats.empty?
      Planet.all.each do |planet|
        planet.update(stats.fetch(planet.name, {}))
      end

      PlanetsCache.write
    end
  end

  every(5.minutes, 'fetch_plots.job') do
    land_stats = AwstatsLands.new.lands

    planets = Planet.all
    rarities = Rarity.all
    lands = Land.all

    land_stats.each do |land_stat|
      planet_name, rarity_name, mint, description, charge, mining, pow, nft, commission, x, y, user, id = land_stat
      land_name = description.split(' on ').first.delete(' ').underscore
      land = lands.find { |l| l.name == land_name }
      planet = planets.find { |p| p.name == planet_name.underscore }
      rarity = rarities.find { |r| r.name ==  rarity_name.underscore}

      plot = Plot.where(planet: planet, coordinate_x: x, coordinate_y: y).first_or_initialize
      plot.attributes = {
        planet: planet,
        land:   land,
        rarity: rarity,
        coordinate_x: x,
        coordinate_y: y,
        owner_profit_share_from_mining: commission,
        land_owned_by: user,
        mint: mint.to_i
      }
      plot.save
    end

    PlotsCache.write
  end
end
