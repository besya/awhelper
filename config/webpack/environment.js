const { environment } = require('@rails/webpacker')
const webpack = require('webpack');

environment.plugins.prepend(
  'Provide',
  new webpack.ProvidePlugin({
    _: 'lodash',
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    React: 'react',
  })
)

module.exports = environment
